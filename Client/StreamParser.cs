﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OmniXtract
{
    public abstract class FileParser : StreamParser
    {
        public FileParser(string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException(fileName + " does not exist.");
            mStream = File.OpenRead(fileName);
        }
    }

    public abstract class StreamParser : IDisposable
    {
        public StreamParser() { }
        public StreamParser(Stream stream)
        {
            mStream = stream;
        }

        protected Stream mStream;

        public abstract char LineDelimiter
        {
            get;
        }

        public char Peek()
        {
            char c = Read();
            if (c != 0)
                mStream.Position--;
            return c;
        }

        public char Read()
        {
            if (mStream == null)
                throw new InvalidOperationException("Stream is not initialized.");
            if (mStream.CanRead)
                return (char)mStream.ReadByte();
            return (char)0;
        }

        public string ReadLine()
        {
            string line = "";
            bool skipLine = false;
            for(char c = Read();  c != 0; c = Read())
            {
                if (c == LineDelimiter)
                    skipLine = true;
                else if (c == '\n')
                {
                    if (skipLine)
                    {
                        skipLine = false;
                        continue;
                    }
                    else
                        break;
                }
                else
                {
                    line += c;
                }
            }
            return line;
        }

        public string ReadQuotedString()
        {
            char quote;
            char c = Peek();
            if (c == '"' || c == '\'')
                quote = c;
            else
                return null;
            string text = "";
            Read();
            c = Read();
            while (c != 0 && c != '\n')
            {
                if (c == quote)
                    break;
                text += c;
                c = Read();
            }
            return text;
        }

        #region IDisposable Support

        bool mDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!mDisposed)
            {
                if (disposing)
                {
                    if (mStream != null)
                    {
                        mStream.Close();
                        mStream.Dispose();
                    }
                }
                mDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
