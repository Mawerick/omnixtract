﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015 - 2018
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Twp.Controls;
using Twp.Data.AO;
using Twp.Utilities;

namespace OmniXtract
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            pluginsBox.Columns.Add(new TreeListColumn("libraryField", "Plugin Library", true));
            pluginsBox.Columns.Add(new TreeListColumn("nameFeld", "Plugin Name", true));
            pluginsBox.Columns.Add(new TreeListColumn("versionField", "Version", 70));

            extractTypesBox.Columns.Add(new TreeListColumn("typeField", "Data type", true));
            extractTypesBox.Columns.Add(new TreeListColumn("idField", "ID", 60));
            extractTypesBox.Columns.Add(new TreeListColumn("progressField", "Progress", 120));
            extractTypesBox.Columns.Add(new TreeListColumn("extractedField", "Extracted", 100));
            extractTypesBox.Columns[3].CellAlignment = ContentAlignment.MiddleRight;

            //debugPage.Enabled = false;
        }

        readonly Color ValidColor = Color.FromArgb(230, 255, 230);
        readonly Color InvalidColor = Color.FromArgb(255, 230, 230);
        AboutInfo mAbout;
        string mCaption;

        Stopwatch mWatch = new Stopwatch();

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            mAbout = new AboutInfo();
            mCaption = string.Format("{0} v{1}", mAbout.Title, VersionString.Format(mAbout.Version, VersionFormat.Simple));
            Text = mCaption;

            gamePathBox.Text = Program.Settings.GamePath;
            extractPathBox.Text = Program.Settings.ExtractPath;
            parsePathBox.Text = Program.Settings.ParsePath;
            RefreshPlugins();

            aboutLabel.Text = mAbout.Title + " v" + VersionString.Format(mAbout.Version, VersionFormat.Simple) + Environment.NewLine
                + "Copyright (C) " + mAbout.Copyright + Environment.NewLine
                + "{{0mawerick@wrongplace.net|mailto:mawerick@wrongplace.net}}" + Environment.NewLine
                + "{{0http://www.wrongplace.net/}}";

#if DEBUG
#if X86
            licenseBox.Text = File.ReadAllText("..\\..\\..\\Dist\\GPLv3_License.txt");
#else
            licenseBox.Text = File.ReadAllText("..\\..\\Dist\\GPLv3_License.txt");
#endif
#else
            licenseBox.Text = File.ReadAllText("GPLv3_License.txt");
#endif
        }

        void UpdateUI(bool extracting, bool parsing)
        {
            bool working = extracting || parsing;

            gamePathBox.Enabled = !working;
            gamePathButton.Enabled = !working;
            parsePathBox.Enabled = !working;
            parsePathButton.Enabled = !working;

            pluginsBox.Enabled = !working;
            refreshPluginsButton.Enabled = !working;

            extractTypesBox.Enabled = !working;
            extractButton.Enabled = !working;
            abortExtractButton.Enabled = extracting;
            abortExtractButton.Visible = extracting;

            parseLayoutTable.Enabled = !working;
            refreshSourcesButton.Enabled = !working;
            includeAllCheckBox.Enabled = !working;
            parseButton.Enabled = !working;
            abortParseButton.Enabled = parsing;
            abortParseButton.Visible = parsing;
        }

        #region Settings Page

        void CheckGamePath()
        {
            if (PathUtils.IsValid(gamePathBox.Text, "AnarchyOnline.exe") || PathUtils.IsValid(gamePathBox.Text, "Clientr.exe"))
            {
                gamePathBox.BackColor = ValidColor;
                var version = AOVersion.Read(gamePathBox.Text);
                if (version != null)
                {
                    versionBox.Text = version.ToString();
                    switch (version.Type)
                    {
                        case 1:
                            clientTypeBox.Text = "Large client";
                            break;
                        case 2:
                            clientTypeBox.Text = "Large client + New Engine";
                            break;
                        default:
                            clientTypeBox.Text = "Small client";
                            break;
                    }
                    if (version.Test >= 0)
                    {
                        clientTypeBox.Text += " (Test server)";
                    }
                }
            }
            else
            {
                gamePathBox.BackColor = InvalidColor;
                versionBox.Text = "N/A";
                clientTypeBox.Text = string.Empty;
            }
            extractVersionBox.Text = versionBox.Text;
            extractClientTypeBox.Text = clientTypeBox.Text;
        }

        void OnGamePathChanged(object sender, EventArgs e)
        {
            Program.Settings.GamePath = gamePathBox.Text;
            CheckGamePath();
        }

        void OnBrowseGamePathClicked(object sender, EventArgs e)
        {
            string path = gamePathBox.Text;
            if (PathUtils.Browse(ref path, "Select the folder where Anarchy Online is installed:"))
                gamePathBox.Text = path;
        }

        void OnRefreshVersionClicked(object sender, EventArgs e)
        {
            CheckGamePath();
        }

        void OnExtractPathChanged(object sender, EventArgs e)
        {
            Program.Settings.ExtractPath = extractPathBox.Text;
            if (Directory.Exists(extractPathBox.Text))
                extractPathBox.BackColor = ValidColor;
            else
                extractPathBox.BackColor = InvalidColor;
            RefreshSourceFiles();
        }

        void OnBrowseExtractPathClicked(object sender, EventArgs e)
        {
            string path = extractPathBox.Text;
            if (PathUtils.Browse(ref path, "Select (or create) a folder to place the extracted files in:", true))
                extractPathBox.Text = path;
        }

        void OnParsePathChanged(object sender, EventArgs e)
        {
            Program.Settings.ParsePath = parsePathBox.Text;
            if (Directory.Exists(parsePathBox.Text))
                parsePathBox.BackColor = ValidColor;
            else
                parsePathBox.BackColor = InvalidColor;
        }

        void OnBrowseParsePathClicked(object sender, EventArgs e)
        {
            string path = parsePathBox.Text;
            if (PathUtils.Browse(ref path, "Select (or create) a folder to place the parsed files in:", true))
                parsePathBox.Text = path;
        }

        void RefreshPlugins()
        {
            PluginManager.Open(); // Load/refresh plugin collection
            pluginErrorIcon.Text = PluginManager.Error;

            pluginsBox.BeginUpdate();
            pluginsBox.Nodes.Clear();
            foreach (PluginItem pluginItem in PluginManager.Plugins)
            {
                var node = new Node();
                node.Fields.Add(pluginItem.BaseName);
                node.Fields.Add(pluginItem.Name);
                node.Fields.Add(pluginItem.Version);
                node.Tag = pluginItem;
                pluginsBox.Nodes.Add(node);
                if (pluginItem.FullName == Program.Settings.Plugin)
                    pluginsBox.FocusedNode = node;
            }
            pluginsBox.EndUpdate();
        }

        private void OnPluginSelected(object sender, EventArgs e)
        {
            Log.Debug("[MainForm.OnPluginSelected] Node: {0}", pluginsBox.FocusedNode);
            Node node = pluginsBox.FocusedNode;
            if (node == null)
                return;

            var pluginItem = node.Tag as PluginItem;
            if (pluginItem == null)
                return;

            Program.Settings.Plugin = pluginItem.FullName;
            PluginManager.Selected = pluginItem;

            Text = mCaption + " :: " + pluginItem.Name;

            RefreshDataTypes();
            RefreshSourceFiles();
        }

        void OnRefreshPluginsClicked(object sender, EventArgs e)
        {
            RefreshPlugins();
        }

        #endregion

        #region Extract Page

        void RefreshDataTypes()
        {
            extractTypesBox.BeginUpdate();
            extractTypesBox.Nodes.Clear();
            if (PluginManager.Selected != null)
            {
                foreach (int type in PluginManager.Selected.Plugin.Types)
                {
                    var node = new Node();
                    node.Fields.Add(ResourceType.GetName(type) + "s");
                    node.Fields.Add(string.Format("0x{0:X2}", type));
                    node.Checked = true;
                    node.Tag = type;
                    extractTypesBox.Nodes.Add(node);
                }
            }
            extractTypesBox.EndUpdate();
        }

        void OnExtractClicked(object sender, EventArgs e)
        {
            string error = null;
            if (string.IsNullOrEmpty(Program.Settings.GamePath))
                error = "No game path is set.";
            else if (string.IsNullOrEmpty(Program.Settings.ExtractPath))
                error = "No output path is set.";
            else if (PluginManager.Selected == null)
                error = "No plugins is selected.";
            if (error != null)
            {
                MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                UpdateUI(true, false);

                // Clear out progress indicators...
                foreach (Node node in extractTypesBox.Nodes)
                {
                    if (node.Checked)
                        node.Fields[2] = "Pending";
                    else
                        node.Fields[2] = "Skipped";
                    node.Fields[3] = null;
                    node.Refresh();
                }

                mWatch.Reset();
                mWatch.Start();
                extractTimer.Start();
                extractWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Debug(ex);
            }
        }

        void OnAbortExtractionClicked(object sender, EventArgs e)
        {
            if (extractWorker.IsBusy)
                extractWorker.CancelAsync();
        }

        private void ExtractWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            worker.ReportProgress(0, "Extracting...");

            var extractor = new Extractor
            {
                UseTransaction = Program.Settings.UseTransactions
            };
            extractor.Initialize(PluginManager.Selected, worker);

            foreach (Node node in extractTypesBox.Nodes)
            {
                if (!node.Checked)
                    continue;

                extractor.Extract(node);
                if (extractor.Aborted || worker.CancellationPending)
                    break;
            }

            if (extractor.Aborted)
            {
                worker.ReportProgress(100, "Aborting...");
                extractor.Abort();
            }
            else
            {
                worker.ReportProgress(100, "Finishing...");
                extractor.Finish();
            }
            e.Result = extractor;
        }

        private void ExtractWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            extractStatus.Text = (string)e.UserState;
            if (e.ProgressPercentage == 100)
                abortExtractButton.Enabled = false;
        }

        private void ExtractWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateUI(false, false);

            var extractor = e.Result as Extractor;

            mWatch.Stop();
            extractTimer.Stop();

            if (extractor.Aborted)
                extractStatus.Text = "Extraction aborted!";
            else
            {
                extractStatus.Text = "Extraction complete!";
                ReportDialog.Show(extractor.Result, mWatch.Elapsed);
                /*
                string time;
                if (mWatch.Elapsed.Hours > 0)
                    time = FormatTime("{0} hours, {1} minutes, {2} seconds.");
                else if (mWatch.Elapsed.Minutes > 0)
                    time = FormatTime("{1} minutes, {2} seconds.");
                else
                    time = FormatTime("{2} seconds.");
                MessageBox.Show("Records extracted:\n\n" + extractor.Result.ToString() + "\nCompleted in " + time,
                    "Extraction complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                */
            }

            RefreshSourceFiles();
        }

        private void ExtractTimer_Tick(object sender, EventArgs e)
        {
            extractTimerLabel.Text = FormatTime("{0:00}:{1:00}:{2:00}");
        }

        #endregion

        #region Parse Page

        void RefreshSourceFiles()
        {
            parseSourceBox.BeginUpdate();
            parseCompareBox.BeginUpdate();

            parseSourceBox.Items.Clear();
            parseCompareBox.Items.Clear();

            if (!string.IsNullOrEmpty(Program.Settings.ExtractPath))
            {
                var di = new DirectoryInfo(Program.Settings.ExtractPath);
                if (di.Exists)
                {
                    FileInfo[] files = null;
                    if (includeAllCheckBox.Checked)
                        files = di.GetFiles("*" + Program.FileExt);
                    else if (PluginManager.Selected != null)
                        files = di.GetFiles(PluginManager.Selected.FullName + "*" + Program.FileExt);
                    if (files != null)
                    {
                        var sources = new List<DataFileInfo>();
                        foreach (var file in files)
                        {
                            if (DataFileInfo.IsValid(file.FullName))
                                sources.Add(new DataFileInfo(file, includeAllCheckBox.Checked));
                        }
                        sources.Sort();
                        foreach (var source in sources)
                        {
                            parseSourceBox.Items.Add(source);
                            parseCompareBox.Items.Add(source);
                        }
                        if (parseSourceBox.Items.Count > 0)
                            parseSourceBox.SelectedIndex = parseSourceBox.Items.Count - 1;
                        if (parseCompareBox.Items.Count > 1)
                            parseCompareBox.SelectedIndex = parseCompareBox.Items.Count - 2;
                        else
                            compareCheckBox.Checked = false;
                    }
                }
            }

            parseSourceBox.EndUpdate();
            parseCompareBox.EndUpdate();
            UpdateParseData();
        }

        private void OnCompareChecked(object sender, EventArgs e)
        {
            parseCompareBox.Enabled = compareCheckBox.Checked;
            if (!parseCompareBox.Enabled)
                parseCompareBox.SelectedIndex = -1;
        }

        private void OnRefreshSourcesClicked(object sender, EventArgs e)
        {
            RefreshSourceFiles();
        }

        private void OnSourceFileSelected(object sender, EventArgs e)
        {
            if (parseCompareBox.Enabled && parseCompareBox.SelectedIndex == parseSourceBox.SelectedIndex)
            {
                int index = parseSourceBox.SelectedIndex;
                if (index < parseCompareBox.Items.Count - 1)
                    ++index;
                else
                    --index;
                parseCompareBox.SelectedIndex = index;
            }
            UpdateParseData();
        }

        private void OnCompareFileSelected(object sender, EventArgs e)
        {
            if (parseCompareBox.SelectedIndex == parseSourceBox.SelectedIndex)
            {
                int index = parseCompareBox.SelectedIndex;
                if (index < parseSourceBox.Items.Count - 1)
                    ++index;
                else
                    --index;
                parseSourceBox.SelectedIndex = index;
            }
            UpdateParseData();
        }

        void UpdateParseData()
        {
            var source = (DataFileInfo)parseSourceBox.SelectedItem;
            if (source != null)
                sourceTextBox.Text = source.File.Name;
            else
                sourceTextBox.Text = "N/A";

            var compare = (DataFileInfo)parseCompareBox.SelectedItem;
            if (compare != null)
                compareTextBox.Text = compare.File.Name;
            else
                compareTextBox.Text = "N/A";

            parseButton.Enabled = source != null;
        }

        void OnParseClicked(object sender, EventArgs e)
        {
            string error = null;
            if (parseSourceBox.SelectedItem == null)
                error = "No source file is selected.";
            else if (compareCheckBox.Checked)
            {
                if (parseCompareBox.SelectedItem == null)
                    error = "No comparison file is selected.";
                else if (parseCompareBox.SelectedItem == parseSourceBox.SelectedItem)
                    error = "Source and comparison file are the same.";
            }

            if (error != null)
            {
                Log.Error(error);
                return;
            }

            try
            {
                UpdateUI(false, true);

                mWatch.Reset();
                mWatch.Start();
                parseTimer.Start();
                parseProgressBar.Visible = true;

                var files = new DataFileInfo[]
                {
                    (DataFileInfo)parseSourceBox.SelectedItem,
                    (DataFileInfo)parseCompareBox.SelectedItem,
                };

                parseWorker.RunWorkerAsync(files);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        void OnAbortParseClicked(object sender, EventArgs e)
        {
            if (parseWorker.IsBusy)
                parseWorker.CancelAsync();
        }

        private void ParseWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            worker.ReportProgress(0, "Parsing...");

            var parser = new Parser();
            parser.ProgressChanged += (s, p) => { worker.ReportProgress(parser.Progress); };
            parser.Message += (s, msg) => { worker.ReportProgress(parser.Progress, msg.Message); };
            e.Result = parser;

            var source = ((DataFileInfo[])e.Argument)[0];
            var compare = (compareCheckBox.Checked) ? ((DataFileInfo[])e.Argument)[1] : null;

#if !DEBUG
            try
            {
#endif
            parser.Initialize(PluginManager.Selected, worker, source, compare);
            parser.Parse();
#if !DEBUG
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                worker.CancelAsync();
            }
#endif

            if (parser.Aborted)
            {
                worker.ReportProgress(100, "Aborting...");
                parser.Abort();
            }
            else
            {
                worker.ReportProgress(100, "Finishing...");
                parser.Finish();
            }
        }

        private void ParseWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            parseProgressBar.Value = e.ProgressPercentage;
            if (e.UserState != null)
                parseStatusLabel.Text = (string)e.UserState;
        }

        private void ParseWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateUI(false, false);

            var parser = e.Result as Parser;
            RefreshFailedList(parser.Failed);

            mWatch.Stop();
            parseTimer.Stop();
            parseProgressBar.Visible = false;
            if (parser.Aborted)
                parseStatusLabel.Text = "Parsing aborted!";
            else
            {
                parseStatusLabel.Text = "Parsing complete!";
                ReportDialog.Show(parser.Result, mWatch.Elapsed, compareCheckBox.Checked);
                /*
                string time;
                if (mWatch.Elapsed.Hours > 0)
                    time = FormatTime("{0} hours, {1} minutes, {2} seconds.");
                else if (mWatch.Elapsed.Minutes > 0)
                    time = FormatTime("{1} minutes, {2} seconds.");
                else
                    time = FormatTime("{2} seconds.");

                string text = "Records parsed:\n\n" + parser.Result.ToString();
                if (parser.Failed != null && parser.Failed.Count > 0)
                {
                    text += "\nFailed records:\t" + parser.Failed.Count.ToString() + "\n";
                }
                text += "\nCompleted in " + time;

                MessageBox.Show(text, "Parsing complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                */
            }
        }

        private void ParseTimer_Tick(object sender, EventArgs e)
        {
            parseTimerLabel.Text = FormatTime("{0:00}:{1:00}:{2:00}");
        }

        string FormatTime(string format)
        {
            return string.Format(format, mWatch.Elapsed.Hours, mWatch.Elapsed.Minutes, mWatch.Elapsed.Seconds);
        }

        #endregion

        #region About page

        private void OnLinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void OnCheckForUpdatesClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var updatesWorker = new BackgroundWorker();
            updatesWorker.DoWork += UpdatesWorker_DoWork;
            updatesWorker.RunWorkerCompleted += UpdatesWorker_RunWorkerCompleted;
            updatesWorker.RunWorkerAsync(mAbout.Version);
            updatesProgressBar.Visible = true;
        }

        private void UpdatesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = Updates.Check("http://www.wrongplace.net/omnixtract.xml", (Version)e.Argument, false);
        }

        private void UpdatesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var newVersion = (Version)e.Result;
            if (newVersion != null)
            {
                string msg = string.Format("A new version of {0} is available.\n" +
                    "Would you like to download it now?\n\n" +
                    "Your version: {1}\n" +
                    "New version: {2}",
                    mAbout.Title, mAbout.Version, newVersion);
                var result = MessageBox.Show(msg, "New version available.", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Process.Start("http://www.wrongplace.net/?id=omnixtract#download");
                }
            }
            else
            {
                MessageBox.Show("You have the latest version of " + mAbout.Title, "No updates available", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            updatesProgressBar.Visible = false;
        }

        #endregion

        #region Debug page

        void RefreshFailedList(List<FailedRecord> records)
        {
            failedList.BeginUpdate();
            failedList.Items.Clear();

            if (records != null && records.Count > 0)
            {
                foreach (var record in records)
                {
                    failedList.Items.Add(record);
                }
                //debugPage.Enabled = true;
                tabControl.Refresh();
            }
            else
            {
                //debugPage.Enabled = false;
                tabControl.Refresh();
            }

            failedList.EndUpdate();
        }

        void OnFailedListSelectionChanged(object sender, EventArgs e)
        {
            if (failedList.SelectedItem == null)
            {
                saveDebugDataButton.Enabled = false;
                hexView.Data = null;
                return;
            }

            var record = (FailedRecord)failedList.SelectedItem;
            hexView.Data = record.Data;
            if (record.Trace != null)
            {
                var sb = new StringBuilder();
                long length = 0;
                foreach (var trace in record.Trace)
                {
                    sb.AppendFormat("{0}: {1} bytes", trace.Type, trace.Length);
                    if (!trace.Success)
                        sb.Append(" (failed)");
                    sb.AppendLine();
                    length += trace.Length;
                }
                if (length < record.Data.LongLength)
                {
                    var remaining = record.Data.LongLength - length;
                    sb.AppendLine("Unknown: " + remaining + " bytes (failed)");
                }
                traceBox.Text = sb.ToString();
            }

            saveDebugDataButton.Enabled = true;
        }

        void OnSaveDebugDataClicked(object sender, EventArgs e)
        {
            var record = (FailedRecord)failedList.SelectedItem;
            saveFileDialog.FileName = record.ToString();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var stream = saveFileDialog.OpenFile())
                {
                    stream.Write(record.Data, 0, record.Data.Length);
                    stream.Close();
                }
                if (record.Trace != null)
                {
                    var json = JsonConvert.SerializeObject(record.Trace, Formatting.Indented);
                    File.WriteAllText(saveFileDialog.FileName + ".trace", json);
                }
            }
        }

        void OnLoadDebugDataClicked(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var file = new FileInfo(openFileDialog.FileName);
                if (file.Exists)
                {
                    var bytes = File.ReadAllBytes(file.FullName);
                    List<ItemParseTrace> trace = null;
                    if (File.Exists(file.FullName + ".trace"))
                        trace = JsonConvert.DeserializeObject<List<ItemParseTrace>>(File.ReadAllText(file.FullName + ".trace"));
                    var record = new FailedRecord(file.Name, bytes, trace);

                    failedList.Items.Add(record);
                    failedList.SelectedItem = record;
                }
            }
        }

        void OnDebugViewChanged(object sender, EventArgs e)
        {
            if (sender is RadioButton button && button.Checked)
            {
                switch (button.Text)
                {
                    case "Hex":
                        hexView.BringToFront();
                        break;

                    case "Trace":
                        traceBox.BringToFront();
                        break;
                }
            }
        }

        #endregion
    }
}
