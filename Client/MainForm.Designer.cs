﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace OmniXtract
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.extractWorker = new System.ComponentModel.BackgroundWorker();
            this.extractTimer = new System.Windows.Forms.Timer(this.components);
            this.parseWorker = new System.ComponentModel.BackgroundWorker();
            this.parseTimer = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl = new OmniXtract.StyledTabControl();
            this.settingsPage = new System.Windows.Forms.TabPage();
            this.pluginErrorIcon = new Twp.Controls.ErrorIcon();
            this.refreshPluginsButton = new System.Windows.Forms.Button();
            this.pluginsBox = new Twp.Controls.TreeListView();
            this.extractPathButton = new System.Windows.Forms.Button();
            this.extractPathLabel = new System.Windows.Forms.Label();
            this.extractPathBox = new System.Windows.Forms.TextBox();
            this.refreshVersionButton = new System.Windows.Forms.Button();
            this.clientTypeBox = new System.Windows.Forms.Label();
            this.versionBox = new System.Windows.Forms.TextBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.gamePathButton = new System.Windows.Forms.Button();
            this.gamePathLabel = new System.Windows.Forms.Label();
            this.gamePathBox = new System.Windows.Forms.TextBox();
            this.parsePathButton = new System.Windows.Forms.Button();
            this.parsePathLabel = new System.Windows.Forms.Label();
            this.parsePathBox = new System.Windows.Forms.TextBox();
            this.extractPage = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.extractClientTypeBox = new System.Windows.Forms.Label();
            this.extractVersionBox = new System.Windows.Forms.TextBox();
            this.extractTimerLabel = new System.Windows.Forms.Label();
            this.extractStatus = new System.Windows.Forms.Label();
            this.abortExtractButton = new System.Windows.Forms.Button();
            this.extractButton = new System.Windows.Forms.Button();
            this.extractTypesBox = new Twp.Controls.TreeListView();
            this.parserPage = new System.Windows.Forms.TabPage();
            this.parseStatusLabel = new System.Windows.Forms.Label();
            this.parseProgressBar = new System.Windows.Forms.ProgressBar();
            this.compareTextLabel = new System.Windows.Forms.Label();
            this.sourceTextLabel = new System.Windows.Forms.Label();
            this.compareTextBox = new System.Windows.Forms.TextBox();
            this.sourceTextBox = new System.Windows.Forms.TextBox();
            this.parseTimerLabel = new System.Windows.Forms.Label();
            this.includeAllCheckBox = new System.Windows.Forms.CheckBox();
            this.parseLayoutTable = new System.Windows.Forms.TableLayoutPanel();
            this.parseSourceBox = new System.Windows.Forms.ListBox();
            this.parseCompareBox = new System.Windows.Forms.ListBox();
            this.compareCheckBox = new System.Windows.Forms.CheckBox();
            this.parseSourceLabel = new System.Windows.Forms.Label();
            this.abortParseButton = new System.Windows.Forms.Button();
            this.parseButton = new System.Windows.Forms.Button();
            this.refreshSourcesButton = new System.Windows.Forms.Button();
            this.aboutPage = new System.Windows.Forms.TabPage();
            this.updatesProgressBar = new System.Windows.Forms.ProgressBar();
            this.checkUpdatesLabel = new System.Windows.Forms.LinkLabel();
            this.aboutLabel = new Twp.Controls.RichLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.licenseBox = new System.Windows.Forms.RichTextBox();
            this.iconBox = new System.Windows.Forms.PictureBox();
            this.debugPage = new System.Windows.Forms.TabPage();
            this.hexViewButton = new System.Windows.Forms.RadioButton();
            this.traceViewButton = new System.Windows.Forms.RadioButton();
            this.loadDebugDataButton = new System.Windows.Forms.Button();
            this.saveDebugDataButton = new System.Windows.Forms.Button();
            this.failedList = new System.Windows.Forms.ListBox();
            this.hexView = new Twp.Controls.HexView();
            this.traceBox = new System.Windows.Forms.TextBox();
            this.tabControl.SuspendLayout();
            this.settingsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pluginsBox)).BeginInit();
            this.extractPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.extractTypesBox)).BeginInit();
            this.parserPage.SuspendLayout();
            this.parseLayoutTable.SuspendLayout();
            this.aboutPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconBox)).BeginInit();
            this.debugPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // extractWorker
            // 
            this.extractWorker.WorkerReportsProgress = true;
            this.extractWorker.WorkerSupportsCancellation = true;
            this.extractWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ExtractWorker_DoWork);
            this.extractWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ExtractWorker_ProgressChanged);
            this.extractWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ExtractWorker_RunWorkerCompleted);
            // 
            // extractTimer
            // 
            this.extractTimer.Tick += new System.EventHandler(this.ExtractTimer_Tick);
            // 
            // parseWorker
            // 
            this.parseWorker.WorkerReportsProgress = true;
            this.parseWorker.WorkerSupportsCancellation = true;
            this.parseWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ParseWorker_DoWork);
            this.parseWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ParseWorker_ProgressChanged);
            this.parseWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ParseWorker_RunWorkerCompleted);
            // 
            // parseTimer
            // 
            this.parseTimer.Tick += new System.EventHandler(this.ParseTimer_Tick);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "bin";
            this.saveFileDialog.Filter = "\"Binary files|*.bin|All files|*.*";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "bin";
            this.openFileDialog.Filter = "\"Binary files|*.bin|All files|*.*";
            // 
            // tabControl
            // 
            this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl.Controls.Add(this.settingsPage);
            this.tabControl.Controls.Add(this.extractPage);
            this.tabControl.Controls.Add(this.parserPage);
            this.tabControl.Controls.Add(this.aboutPage);
            this.tabControl.Controls.Add(this.debugPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ItemSize = new System.Drawing.Size(60, 80);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(644, 305);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            // 
            // settingsPage
            // 
            this.settingsPage.Controls.Add(this.pluginErrorIcon);
            this.settingsPage.Controls.Add(this.refreshPluginsButton);
            this.settingsPage.Controls.Add(this.pluginsBox);
            this.settingsPage.Controls.Add(this.extractPathButton);
            this.settingsPage.Controls.Add(this.extractPathLabel);
            this.settingsPage.Controls.Add(this.extractPathBox);
            this.settingsPage.Controls.Add(this.refreshVersionButton);
            this.settingsPage.Controls.Add(this.clientTypeBox);
            this.settingsPage.Controls.Add(this.versionBox);
            this.settingsPage.Controls.Add(this.versionLabel);
            this.settingsPage.Controls.Add(this.gamePathButton);
            this.settingsPage.Controls.Add(this.gamePathLabel);
            this.settingsPage.Controls.Add(this.gamePathBox);
            this.settingsPage.Controls.Add(this.parsePathButton);
            this.settingsPage.Controls.Add(this.parsePathLabel);
            this.settingsPage.Controls.Add(this.parsePathBox);
            this.settingsPage.Location = new System.Drawing.Point(84, 4);
            this.settingsPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.settingsPage.Name = "settingsPage";
            this.settingsPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.settingsPage.Size = new System.Drawing.Size(556, 297);
            this.settingsPage.TabIndex = 3;
            this.settingsPage.Text = "Settings";
            // 
            // pluginErrorIcon
            // 
            this.pluginErrorIcon.Location = new System.Drawing.Point(466, 141);
            this.pluginErrorIcon.Name = "pluginErrorIcon";
            this.pluginErrorIcon.Size = new System.Drawing.Size(16, 16);
            this.pluginErrorIcon.TabIndex = 17;
            // 
            // refreshPluginsButton
            // 
            this.refreshPluginsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.refreshPluginsButton.Location = new System.Drawing.Point(466, 266);
            this.refreshPluginsButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.refreshPluginsButton.Name = "refreshPluginsButton";
            this.refreshPluginsButton.Size = new System.Drawing.Size(87, 27);
            this.refreshPluginsButton.TabIndex = 16;
            this.refreshPluginsButton.Text = "&Refresh";
            this.refreshPluginsButton.UseVisualStyleBackColor = true;
            this.refreshPluginsButton.Click += new System.EventHandler(this.OnRefreshPluginsClicked);
            // 
            // pluginsBox
            // 
            this.pluginsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pluginsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pluginsBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pluginsBox.Location = new System.Drawing.Point(3, 139);
            this.pluginsBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pluginsBox.MultiSelect = false;
            this.pluginsBox.Name = "pluginsBox";
            this.pluginsBox.ShowLines = false;
            this.pluginsBox.ShowPlusMinus = false;
            this.pluginsBox.Size = new System.Drawing.Size(457, 154);
            this.pluginsBox.TabIndex = 15;
            this.pluginsBox.AfterSelect += new System.EventHandler(this.OnPluginSelected);
            // 
            // extractPathButton
            // 
            this.extractPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.extractPathButton.Location = new System.Drawing.Point(466, 72);
            this.extractPathButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractPathButton.Name = "extractPathButton";
            this.extractPathButton.Size = new System.Drawing.Size(87, 27);
            this.extractPathButton.TabIndex = 9;
            this.extractPathButton.Text = "Bro&wse";
            this.extractPathButton.UseVisualStyleBackColor = true;
            this.extractPathButton.Click += new System.EventHandler(this.OnBrowseExtractPathClicked);
            // 
            // extractPathLabel
            // 
            this.extractPathLabel.AutoSize = true;
            this.extractPathLabel.Location = new System.Drawing.Point(6, 77);
            this.extractPathLabel.Name = "extractPathLabel";
            this.extractPathLabel.Size = new System.Drawing.Size(88, 16);
            this.extractPathLabel.TabIndex = 7;
            this.extractPathLabel.Text = "Extract folder:";
            // 
            // extractPathBox
            // 
            this.extractPathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.extractPathBox.Location = new System.Drawing.Point(113, 74);
            this.extractPathBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractPathBox.Name = "extractPathBox";
            this.extractPathBox.Size = new System.Drawing.Size(347, 22);
            this.extractPathBox.TabIndex = 8;
            this.extractPathBox.TextChanged += new System.EventHandler(this.OnExtractPathChanged);
            // 
            // refreshVersionButton
            // 
            this.refreshVersionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.refreshVersionButton.Location = new System.Drawing.Point(466, 38);
            this.refreshVersionButton.Name = "refreshVersionButton";
            this.refreshVersionButton.Size = new System.Drawing.Size(87, 27);
            this.refreshVersionButton.TabIndex = 6;
            this.refreshVersionButton.Text = "&Refresh";
            this.refreshVersionButton.UseVisualStyleBackColor = true;
            this.refreshVersionButton.Click += new System.EventHandler(this.OnRefreshVersionClicked);
            // 
            // clientTypeBox
            // 
            this.clientTypeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clientTypeBox.Location = new System.Drawing.Point(188, 43);
            this.clientTypeBox.Name = "clientTypeBox";
            this.clientTypeBox.Size = new System.Drawing.Size(272, 21);
            this.clientTypeBox.TabIndex = 5;
            // 
            // versionBox
            // 
            this.versionBox.Location = new System.Drawing.Point(113, 40);
            this.versionBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.versionBox.Name = "versionBox";
            this.versionBox.ReadOnly = true;
            this.versionBox.Size = new System.Drawing.Size(69, 22);
            this.versionBox.TabIndex = 4;
            this.versionBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Location = new System.Drawing.Point(6, 43);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(57, 16);
            this.versionLabel.TabIndex = 3;
            this.versionLabel.Text = "Version:";
            // 
            // gamePathButton
            // 
            this.gamePathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gamePathButton.Location = new System.Drawing.Point(466, 4);
            this.gamePathButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gamePathButton.Name = "gamePathButton";
            this.gamePathButton.Size = new System.Drawing.Size(87, 27);
            this.gamePathButton.TabIndex = 2;
            this.gamePathButton.Text = "&Browse";
            this.gamePathButton.UseVisualStyleBackColor = true;
            this.gamePathButton.Click += new System.EventHandler(this.OnBrowseGamePathClicked);
            // 
            // gamePathLabel
            // 
            this.gamePathLabel.AutoSize = true;
            this.gamePathLabel.Location = new System.Drawing.Point(6, 9);
            this.gamePathLabel.Name = "gamePathLabel";
            this.gamePathLabel.Size = new System.Drawing.Size(67, 16);
            this.gamePathLabel.TabIndex = 0;
            this.gamePathLabel.Text = "AO folder:";
            // 
            // gamePathBox
            // 
            this.gamePathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gamePathBox.Location = new System.Drawing.Point(113, 6);
            this.gamePathBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gamePathBox.Name = "gamePathBox";
            this.gamePathBox.Size = new System.Drawing.Size(347, 22);
            this.gamePathBox.TabIndex = 1;
            this.gamePathBox.TextChanged += new System.EventHandler(this.OnGamePathChanged);
            // 
            // parsePathButton
            // 
            this.parsePathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.parsePathButton.Location = new System.Drawing.Point(466, 107);
            this.parsePathButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.parsePathButton.Name = "parsePathButton";
            this.parsePathButton.Size = new System.Drawing.Size(87, 27);
            this.parsePathButton.TabIndex = 13;
            this.parsePathButton.Text = "Brow&se";
            this.parsePathButton.UseVisualStyleBackColor = true;
            this.parsePathButton.Click += new System.EventHandler(this.OnBrowseParsePathClicked);
            // 
            // parsePathLabel
            // 
            this.parsePathLabel.AutoSize = true;
            this.parsePathLabel.Location = new System.Drawing.Point(6, 112);
            this.parsePathLabel.Name = "parsePathLabel";
            this.parsePathLabel.Size = new System.Drawing.Size(84, 16);
            this.parsePathLabel.TabIndex = 11;
            this.parsePathLabel.Text = "Parse folder:";
            // 
            // parsePathBox
            // 
            this.parsePathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parsePathBox.Location = new System.Drawing.Point(113, 109);
            this.parsePathBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.parsePathBox.Name = "parsePathBox";
            this.parsePathBox.Size = new System.Drawing.Size(347, 22);
            this.parsePathBox.TabIndex = 12;
            this.parsePathBox.TextChanged += new System.EventHandler(this.OnParsePathChanged);
            // 
            // extractPage
            // 
            this.extractPage.Controls.Add(this.label2);
            this.extractPage.Controls.Add(this.extractClientTypeBox);
            this.extractPage.Controls.Add(this.extractVersionBox);
            this.extractPage.Controls.Add(this.extractTimerLabel);
            this.extractPage.Controls.Add(this.extractStatus);
            this.extractPage.Controls.Add(this.abortExtractButton);
            this.extractPage.Controls.Add(this.extractButton);
            this.extractPage.Controls.Add(this.extractTypesBox);
            this.extractPage.Location = new System.Drawing.Point(84, 4);
            this.extractPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractPage.Name = "extractPage";
            this.extractPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractPage.Size = new System.Drawing.Size(556, 297);
            this.extractPage.TabIndex = 0;
            this.extractPage.Text = "Extract";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Selected AO Version:";
            // 
            // extractClientTypeBox
            // 
            this.extractClientTypeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.extractClientTypeBox.Location = new System.Drawing.Point(223, 9);
            this.extractClientTypeBox.Name = "extractClientTypeBox";
            this.extractClientTypeBox.Size = new System.Drawing.Size(330, 19);
            this.extractClientTypeBox.TabIndex = 2;
            // 
            // extractVersionBox
            // 
            this.extractVersionBox.Location = new System.Drawing.Point(148, 6);
            this.extractVersionBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractVersionBox.Name = "extractVersionBox";
            this.extractVersionBox.ReadOnly = true;
            this.extractVersionBox.Size = new System.Drawing.Size(69, 22);
            this.extractVersionBox.TabIndex = 1;
            this.extractVersionBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // extractTimerLabel
            // 
            this.extractTimerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.extractTimerLabel.Location = new System.Drawing.Point(395, 269);
            this.extractTimerLabel.Name = "extractTimerLabel";
            this.extractTimerLabel.Size = new System.Drawing.Size(65, 20);
            this.extractTimerLabel.TabIndex = 5;
            this.extractTimerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // extractStatus
            // 
            this.extractStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.extractStatus.Location = new System.Drawing.Point(6, 269);
            this.extractStatus.Name = "extractStatus";
            this.extractStatus.Size = new System.Drawing.Size(383, 20);
            this.extractStatus.TabIndex = 4;
            this.extractStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // abortExtractButton
            // 
            this.abortExtractButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.abortExtractButton.Location = new System.Drawing.Point(466, 266);
            this.abortExtractButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.abortExtractButton.Name = "abortExtractButton";
            this.abortExtractButton.Size = new System.Drawing.Size(87, 27);
            this.abortExtractButton.TabIndex = 6;
            this.abortExtractButton.Text = "&Abort";
            this.abortExtractButton.UseVisualStyleBackColor = true;
            this.abortExtractButton.Visible = false;
            this.abortExtractButton.Click += new System.EventHandler(this.OnAbortExtractionClicked);
            // 
            // extractButton
            // 
            this.extractButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.extractButton.Location = new System.Drawing.Point(466, 266);
            this.extractButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractButton.Name = "extractButton";
            this.extractButton.Size = new System.Drawing.Size(87, 27);
            this.extractButton.TabIndex = 4;
            this.extractButton.Text = "&Extract";
            this.extractButton.UseVisualStyleBackColor = true;
            this.extractButton.Click += new System.EventHandler(this.OnExtractClicked);
            // 
            // extractTypesBox
            // 
            this.extractTypesBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.extractTypesBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.extractTypesBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.extractTypesBox.Location = new System.Drawing.Point(3, 34);
            this.extractTypesBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.extractTypesBox.Name = "extractTypesBox";
            this.extractTypesBox.ShowCheckBoxes = true;
            this.extractTypesBox.ShowLines = false;
            this.extractTypesBox.ShowPlusMinus = false;
            this.extractTypesBox.Size = new System.Drawing.Size(550, 227);
            this.extractTypesBox.TabIndex = 3;
            // 
            // parserPage
            // 
            this.parserPage.Controls.Add(this.parseStatusLabel);
            this.parserPage.Controls.Add(this.parseProgressBar);
            this.parserPage.Controls.Add(this.compareTextLabel);
            this.parserPage.Controls.Add(this.sourceTextLabel);
            this.parserPage.Controls.Add(this.compareTextBox);
            this.parserPage.Controls.Add(this.sourceTextBox);
            this.parserPage.Controls.Add(this.parseTimerLabel);
            this.parserPage.Controls.Add(this.includeAllCheckBox);
            this.parserPage.Controls.Add(this.parseLayoutTable);
            this.parserPage.Controls.Add(this.abortParseButton);
            this.parserPage.Controls.Add(this.parseButton);
            this.parserPage.Controls.Add(this.refreshSourcesButton);
            this.parserPage.Location = new System.Drawing.Point(84, 4);
            this.parserPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.parserPage.Name = "parserPage";
            this.parserPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.parserPage.Size = new System.Drawing.Size(556, 297);
            this.parserPage.TabIndex = 1;
            this.parserPage.Text = "Parse";
            // 
            // parseStatusLabel
            // 
            this.parseStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parseStatusLabel.Location = new System.Drawing.Point(9, 269);
            this.parseStatusLabel.Name = "parseStatusLabel";
            this.parseStatusLabel.Size = new System.Drawing.Size(274, 20);
            this.parseStatusLabel.TabIndex = 7;
            // 
            // parseProgressBar
            // 
            this.parseProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.parseProgressBar.Location = new System.Drawing.Point(289, 269);
            this.parseProgressBar.Name = "parseProgressBar";
            this.parseProgressBar.Size = new System.Drawing.Size(100, 21);
            this.parseProgressBar.TabIndex = 8;
            this.parseProgressBar.Visible = false;
            // 
            // compareTextLabel
            // 
            this.compareTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.compareTextLabel.AutoSize = true;
            this.compareTextLabel.Location = new System.Drawing.Point(9, 241);
            this.compareTextLabel.Name = "compareTextLabel";
            this.compareTextLabel.Size = new System.Drawing.Size(87, 16);
            this.compareTextLabel.TabIndex = 5;
            this.compareTextLabel.Text = "Compare file:";
            // 
            // sourceTextLabel
            // 
            this.sourceTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sourceTextLabel.AutoSize = true;
            this.sourceTextLabel.Location = new System.Drawing.Point(9, 213);
            this.sourceTextLabel.Name = "sourceTextLabel";
            this.sourceTextLabel.Size = new System.Drawing.Size(74, 16);
            this.sourceTextLabel.TabIndex = 3;
            this.sourceTextLabel.Text = "Source file:";
            // 
            // compareTextBox
            // 
            this.compareTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.compareTextBox.Location = new System.Drawing.Point(102, 238);
            this.compareTextBox.Name = "compareTextBox";
            this.compareTextBox.ReadOnly = true;
            this.compareTextBox.Size = new System.Drawing.Size(355, 22);
            this.compareTextBox.TabIndex = 6;
            // 
            // sourceTextBox
            // 
            this.sourceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceTextBox.Location = new System.Drawing.Point(102, 210);
            this.sourceTextBox.Name = "sourceTextBox";
            this.sourceTextBox.ReadOnly = true;
            this.sourceTextBox.Size = new System.Drawing.Size(355, 22);
            this.sourceTextBox.TabIndex = 4;
            // 
            // parseTimerLabel
            // 
            this.parseTimerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.parseTimerLabel.Location = new System.Drawing.Point(395, 269);
            this.parseTimerLabel.Name = "parseTimerLabel";
            this.parseTimerLabel.Size = new System.Drawing.Size(65, 20);
            this.parseTimerLabel.TabIndex = 9;
            this.parseTimerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // includeAllCheckBox
            // 
            this.includeAllCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.includeAllCheckBox.AutoSize = true;
            this.includeAllCheckBox.Location = new System.Drawing.Point(466, 38);
            this.includeAllCheckBox.Name = "includeAllCheckBox";
            this.includeAllCheckBox.Size = new System.Drawing.Size(87, 20);
            this.includeAllCheckBox.TabIndex = 2;
            this.includeAllCheckBox.Text = "Include all";
            this.includeAllCheckBox.UseVisualStyleBackColor = true;
            // 
            // parseLayoutTable
            // 
            this.parseLayoutTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parseLayoutTable.ColumnCount = 2;
            this.parseLayoutTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.parseLayoutTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.parseLayoutTable.Controls.Add(this.parseSourceBox, 0, 1);
            this.parseLayoutTable.Controls.Add(this.parseCompareBox, 1, 1);
            this.parseLayoutTable.Controls.Add(this.compareCheckBox, 1, 0);
            this.parseLayoutTable.Controls.Add(this.parseSourceLabel, 0, 0);
            this.parseLayoutTable.Location = new System.Drawing.Point(0, 4);
            this.parseLayoutTable.Margin = new System.Windows.Forms.Padding(0);
            this.parseLayoutTable.Name = "parseLayoutTable";
            this.parseLayoutTable.RowCount = 2;
            this.parseLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.parseLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.parseLayoutTable.Size = new System.Drawing.Size(460, 203);
            this.parseLayoutTable.TabIndex = 0;
            // 
            // parseSourceBox
            // 
            this.parseSourceBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parseSourceBox.FormattingEnabled = true;
            this.parseSourceBox.IntegralHeight = false;
            this.parseSourceBox.ItemHeight = 16;
            this.parseSourceBox.Location = new System.Drawing.Point(3, 29);
            this.parseSourceBox.Name = "parseSourceBox";
            this.parseSourceBox.Size = new System.Drawing.Size(224, 171);
            this.parseSourceBox.TabIndex = 1;
            this.parseSourceBox.SelectedIndexChanged += new System.EventHandler(this.OnSourceFileSelected);
            // 
            // parseCompareBox
            // 
            this.parseCompareBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parseCompareBox.FormattingEnabled = true;
            this.parseCompareBox.IntegralHeight = false;
            this.parseCompareBox.ItemHeight = 16;
            this.parseCompareBox.Location = new System.Drawing.Point(233, 29);
            this.parseCompareBox.Name = "parseCompareBox";
            this.parseCompareBox.Size = new System.Drawing.Size(224, 171);
            this.parseCompareBox.TabIndex = 3;
            this.parseCompareBox.SelectedIndexChanged += new System.EventHandler(this.OnCompareFileSelected);
            // 
            // compareCheckBox
            // 
            this.compareCheckBox.AutoSize = true;
            this.compareCheckBox.Checked = true;
            this.compareCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.compareCheckBox.Location = new System.Drawing.Point(233, 3);
            this.compareCheckBox.Name = "compareCheckBox";
            this.compareCheckBox.Size = new System.Drawing.Size(111, 20);
            this.compareCheckBox.TabIndex = 2;
            this.compareCheckBox.Text = "Compare with:";
            this.compareCheckBox.UseVisualStyleBackColor = true;
            this.compareCheckBox.CheckedChanged += new System.EventHandler(this.OnCompareChecked);
            // 
            // parseSourceLabel
            // 
            this.parseSourceLabel.AutoSize = true;
            this.parseSourceLabel.Location = new System.Drawing.Point(6, 5);
            this.parseSourceLabel.Margin = new System.Windows.Forms.Padding(6, 5, 3, 3);
            this.parseSourceLabel.Name = "parseSourceLabel";
            this.parseSourceLabel.Size = new System.Drawing.Size(74, 16);
            this.parseSourceLabel.TabIndex = 0;
            this.parseSourceLabel.Text = "Source file:";
            // 
            // abortParseButton
            // 
            this.abortParseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.abortParseButton.Enabled = false;
            this.abortParseButton.Location = new System.Drawing.Point(466, 266);
            this.abortParseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.abortParseButton.Name = "abortParseButton";
            this.abortParseButton.Size = new System.Drawing.Size(87, 27);
            this.abortParseButton.TabIndex = 10;
            this.abortParseButton.Text = "&Abort";
            this.abortParseButton.UseVisualStyleBackColor = true;
            this.abortParseButton.Visible = false;
            this.abortParseButton.Click += new System.EventHandler(this.OnAbortParseClicked);
            // 
            // parseButton
            // 
            this.parseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.parseButton.Location = new System.Drawing.Point(466, 266);
            this.parseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.parseButton.Name = "parseButton";
            this.parseButton.Size = new System.Drawing.Size(87, 27);
            this.parseButton.TabIndex = 11;
            this.parseButton.Text = "&Parse";
            this.parseButton.UseVisualStyleBackColor = true;
            this.parseButton.Click += new System.EventHandler(this.OnParseClicked);
            // 
            // refreshSourcesButton
            // 
            this.refreshSourcesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.refreshSourcesButton.Location = new System.Drawing.Point(466, 4);
            this.refreshSourcesButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.refreshSourcesButton.Name = "refreshSourcesButton";
            this.refreshSourcesButton.Size = new System.Drawing.Size(87, 27);
            this.refreshSourcesButton.TabIndex = 1;
            this.refreshSourcesButton.Text = "&Refresh";
            this.refreshSourcesButton.UseVisualStyleBackColor = true;
            this.refreshSourcesButton.Click += new System.EventHandler(this.OnRefreshSourcesClicked);
            // 
            // aboutPage
            // 
            this.aboutPage.Controls.Add(this.updatesProgressBar);
            this.aboutPage.Controls.Add(this.checkUpdatesLabel);
            this.aboutPage.Controls.Add(this.aboutLabel);
            this.aboutPage.Controls.Add(this.pictureBox1);
            this.aboutPage.Controls.Add(this.licenseBox);
            this.aboutPage.Controls.Add(this.iconBox);
            this.aboutPage.Location = new System.Drawing.Point(84, 4);
            this.aboutPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.aboutPage.Name = "aboutPage";
            this.aboutPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.aboutPage.Size = new System.Drawing.Size(556, 297);
            this.aboutPage.TabIndex = 4;
            this.aboutPage.Text = "About";
            // 
            // updatesProgressBar
            // 
            this.updatesProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.updatesProgressBar.Location = new System.Drawing.Point(435, 52);
            this.updatesProgressBar.Name = "updatesProgressBar";
            this.updatesProgressBar.Size = new System.Drawing.Size(116, 16);
            this.updatesProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.updatesProgressBar.TabIndex = 5;
            this.updatesProgressBar.Visible = false;
            // 
            // checkUpdatesLabel
            // 
            this.checkUpdatesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkUpdatesLabel.AutoSize = true;
            this.checkUpdatesLabel.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.checkUpdatesLabel.Location = new System.Drawing.Point(434, 51);
            this.checkUpdatesLabel.Name = "checkUpdatesLabel";
            this.checkUpdatesLabel.Size = new System.Drawing.Size(119, 16);
            this.checkUpdatesLabel.TabIndex = 6;
            this.checkUpdatesLabel.TabStop = true;
            this.checkUpdatesLabel.Text = "Check for Updates";
            this.checkUpdatesLabel.VisitedLinkColor = System.Drawing.SystemColors.HotTrack;
            this.checkUpdatesLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnCheckForUpdatesClicked);
            // 
            // aboutLabel
            // 
            this.aboutLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutLabel.Location = new System.Drawing.Point(73, 5);
            this.aboutLabel.Name = "aboutLabel";
            this.aboutLabel.Size = new System.Drawing.Size(358, 62);
            this.aboutLabel.Splitters = new string[] {
        "{{",
        "}}"};
            this.aboutLabel.TabIndex = 3;
            this.aboutLabel.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.OnLinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::OmniXtract.Properties.Resources.GPL;
            this.pictureBox1.Location = new System.Drawing.Point(465, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(88, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // licenseBox
            // 
            this.licenseBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.licenseBox.Location = new System.Drawing.Point(3, 73);
            this.licenseBox.Name = "licenseBox";
            this.licenseBox.Size = new System.Drawing.Size(550, 221);
            this.licenseBox.TabIndex = 1;
            this.licenseBox.Text = "";
            this.licenseBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.OnLinkClicked);
            // 
            // iconBox
            // 
            this.iconBox.Image = global::OmniXtract.Properties.Resources.Icon_64;
            this.iconBox.Location = new System.Drawing.Point(3, 3);
            this.iconBox.Name = "iconBox";
            this.iconBox.Size = new System.Drawing.Size(64, 64);
            this.iconBox.TabIndex = 0;
            this.iconBox.TabStop = false;
            // 
            // debugPage
            // 
            this.debugPage.Controls.Add(this.hexViewButton);
            this.debugPage.Controls.Add(this.traceViewButton);
            this.debugPage.Controls.Add(this.loadDebugDataButton);
            this.debugPage.Controls.Add(this.saveDebugDataButton);
            this.debugPage.Controls.Add(this.failedList);
            this.debugPage.Controls.Add(this.hexView);
            this.debugPage.Controls.Add(this.traceBox);
            this.debugPage.Location = new System.Drawing.Point(84, 4);
            this.debugPage.Name = "debugPage";
            this.debugPage.Padding = new System.Windows.Forms.Padding(3);
            this.debugPage.Size = new System.Drawing.Size(556, 297);
            this.debugPage.TabIndex = 5;
            this.debugPage.Text = "Debug";
            // 
            // hexViewButton
            // 
            this.hexViewButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.hexViewButton.Checked = true;
            this.hexViewButton.Location = new System.Drawing.Point(377, 4);
            this.hexViewButton.Name = "hexViewButton";
            this.hexViewButton.Size = new System.Drawing.Size(85, 27);
            this.hexViewButton.TabIndex = 8;
            this.hexViewButton.TabStop = true;
            this.hexViewButton.Text = "Hex";
            this.hexViewButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.hexViewButton.UseVisualStyleBackColor = true;
            this.hexViewButton.CheckedChanged += new System.EventHandler(this.OnDebugViewChanged);
            // 
            // traceViewButton
            // 
            this.traceViewButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.traceViewButton.Location = new System.Drawing.Point(468, 4);
            this.traceViewButton.Name = "traceViewButton";
            this.traceViewButton.Size = new System.Drawing.Size(85, 27);
            this.traceViewButton.TabIndex = 7;
            this.traceViewButton.Text = "Trace";
            this.traceViewButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.traceViewButton.UseVisualStyleBackColor = true;
            this.traceViewButton.CheckedChanged += new System.EventHandler(this.OnDebugViewChanged);
            // 
            // loadDebugDataButton
            // 
            this.loadDebugDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loadDebugDataButton.Location = new System.Drawing.Point(68, 4);
            this.loadDebugDataButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.loadDebugDataButton.Name = "loadDebugDataButton";
            this.loadDebugDataButton.Size = new System.Drawing.Size(60, 27);
            this.loadDebugDataButton.TabIndex = 6;
            this.loadDebugDataButton.Text = "&Load";
            this.loadDebugDataButton.UseVisualStyleBackColor = true;
            this.loadDebugDataButton.Click += new System.EventHandler(this.OnLoadDebugDataClicked);
            // 
            // saveDebugDataButton
            // 
            this.saveDebugDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveDebugDataButton.Enabled = false;
            this.saveDebugDataButton.Location = new System.Drawing.Point(3, 4);
            this.saveDebugDataButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.saveDebugDataButton.Name = "saveDebugDataButton";
            this.saveDebugDataButton.Size = new System.Drawing.Size(60, 27);
            this.saveDebugDataButton.TabIndex = 5;
            this.saveDebugDataButton.Text = "&Save";
            this.saveDebugDataButton.UseVisualStyleBackColor = true;
            this.saveDebugDataButton.Click += new System.EventHandler(this.OnSaveDebugDataClicked);
            // 
            // failedList
            // 
            this.failedList.FormattingEnabled = true;
            this.failedList.IntegralHeight = false;
            this.failedList.ItemHeight = 16;
            this.failedList.Location = new System.Drawing.Point(3, 38);
            this.failedList.Name = "failedList";
            this.failedList.Size = new System.Drawing.Size(125, 256);
            this.failedList.TabIndex = 0;
            this.failedList.SelectedIndexChanged += new System.EventHandler(this.OnFailedListSelectionChanged);
            // 
            // hexView
            // 
            this.hexView.Data = null;
            this.hexView.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hexView.Location = new System.Drawing.Point(134, 38);
            this.hexView.MaxColumns = 12;
            this.hexView.Name = "hexView";
            this.hexView.Padding = new System.Windows.Forms.Padding(2);
            this.hexView.ShowHeader = false;
            this.hexView.Size = new System.Drawing.Size(419, 256);
            this.hexView.TabIndex = 4;
            // 
            // traceBox
            // 
            this.traceBox.Location = new System.Drawing.Point(134, 38);
            this.traceBox.Multiline = true;
            this.traceBox.Name = "traceBox";
            this.traceBox.ReadOnly = true;
            this.traceBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.traceBox.Size = new System.Drawing.Size(419, 256);
            this.traceBox.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 305);
            this.Controls.Add(this.tabControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.Text = "OmniXtract";
            this.tabControl.ResumeLayout(false);
            this.settingsPage.ResumeLayout(false);
            this.settingsPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pluginsBox)).EndInit();
            this.extractPage.ResumeLayout(false);
            this.extractPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.extractTypesBox)).EndInit();
            this.parserPage.ResumeLayout(false);
            this.parserPage.PerformLayout();
            this.parseLayoutTable.ResumeLayout(false);
            this.parseLayoutTable.PerformLayout();
            this.aboutPage.ResumeLayout(false);
            this.aboutPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconBox)).EndInit();
            this.debugPage.ResumeLayout(false);
            this.debugPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button gamePathButton;
        private System.Windows.Forms.Label gamePathLabel;
        private System.Windows.Forms.TextBox gamePathBox;
        private System.Windows.Forms.TextBox parsePathBox;
        private System.Windows.Forms.Label parsePathLabel;
        private System.Windows.Forms.Button parsePathButton;
        private System.Windows.Forms.Button abortExtractButton;
        private StyledTabControl tabControl;
        private System.Windows.Forms.TabPage parserPage;
        private System.Windows.Forms.TabPage extractPage;
        private Twp.Controls.TreeListView extractTypesBox;
        private System.Windows.Forms.Button extractButton;
        private System.Windows.Forms.Label extractStatus;
        private System.Windows.Forms.TabPage settingsPage;
        private System.Windows.Forms.TextBox versionBox;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label clientTypeBox;
        private System.Windows.Forms.TabPage aboutPage;
        private System.ComponentModel.BackgroundWorker extractWorker;
        private System.Windows.Forms.Timer extractTimer;
        private System.Windows.Forms.Label extractTimerLabel;
        private System.Windows.Forms.ListBox parseSourceBox;
        private System.Windows.Forms.Label parseSourceLabel;
        private System.Windows.Forms.ListBox parseCompareBox;
        private System.Windows.Forms.CheckBox compareCheckBox;
        private System.Windows.Forms.Button abortParseButton;
        private System.Windows.Forms.Button parseButton;
        private System.Windows.Forms.Button refreshSourcesButton;
        private System.Windows.Forms.TableLayoutPanel parseLayoutTable;
        private System.Windows.Forms.CheckBox includeAllCheckBox;
        private System.ComponentModel.BackgroundWorker parseWorker;
        private System.Windows.Forms.Timer parseTimer;
        private System.Windows.Forms.Label parseTimerLabel;
        private System.Windows.Forms.Label compareTextLabel;
        private System.Windows.Forms.Label sourceTextLabel;
        private System.Windows.Forms.TextBox compareTextBox;
        private System.Windows.Forms.TextBox sourceTextBox;
        private System.Windows.Forms.Button refreshVersionButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label extractClientTypeBox;
        private System.Windows.Forms.TextBox extractVersionBox;
        private System.Windows.Forms.Button extractPathButton;
        private System.Windows.Forms.Label extractPathLabel;
        private System.Windows.Forms.TextBox extractPathBox;
        private System.Windows.Forms.TabPage debugPage;
        private System.Windows.Forms.ListBox failedList;
        private System.Windows.Forms.Button refreshPluginsButton;
        private Twp.Controls.TreeListView pluginsBox;
        private System.Windows.Forms.PictureBox iconBox;
        private System.Windows.Forms.RichTextBox licenseBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Twp.Controls.RichLabel aboutLabel;
        private System.Windows.Forms.ProgressBar updatesProgressBar;
        private System.Windows.Forms.LinkLabel checkUpdatesLabel;
        private System.Windows.Forms.Label parseStatusLabel;
        private System.Windows.Forms.ProgressBar parseProgressBar;
        private Twp.Controls.ErrorIcon pluginErrorIcon;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.TextBox traceBox;
        private Twp.Controls.HexView hexView;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.RadioButton hexViewButton;
        private System.Windows.Forms.RadioButton traceViewButton;
        private System.Windows.Forms.Button loadDebugDataButton;
        private System.Windows.Forms.Button saveDebugDataButton;
    }
}

