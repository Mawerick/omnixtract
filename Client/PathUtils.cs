﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.IO;

namespace OmniXtract
{
    public static class PathUtils
    {
        /// <summary>
        /// Attempts to confirm the validity of a path.
        /// A Folder Browser dialog is presented repeatedly until the user either
        /// selects a valid path or clicks the "Cancel" button.
        /// </summary>
        /// <param name="path">A string representing the path. If this is not null or an empty string,
        /// it is used to provide the dialog with a default selection.</param>
        /// <param name="text">A string represeting the text to display in the dialog.</param>
        /// <param name="fileName">A string represeting the name of a file to check for in the path.</param>
        /// <returns>true if a valid path is found, otherwise, false.</returns>
        public static bool Confirm(ref string path, string text, string fileName)
        {
            return Confirm(ref path, text, fileName, false);
        }

        /// <summary>
        /// Attempts to confirm the validity of a path.
        /// A Folder Browser dialog is presented repeatedly until the user either
        /// selects a valid path or clicks the "Cancel" button.
        /// </summary>
        /// <param name="path">A string representing the path. If this is not null or an empty string,
        /// it is used to provide the dialog with a default selection.</param>
        /// <param name="text">A string represeting the text to display in the dialog.</param>
        /// <param name="fileName">A string represeting the name of a file to check for in the path.</param>
        /// <param name="showNew">A value indicating whether the New Folder button appears in the dialog.</param>
        /// <returns>true if a valid path is found, otherwise, false.</returns>
        public static bool Confirm(ref string path, string text, string fileName, bool showNew)
        {
            while (!IsValid(path, fileName))
            {
                if (Browse(ref path, text, showNew) == false)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Displays a <see cref="System.Windows.Forms.FolderBrowserDialog"/> to the user.
        /// No path validation is performed.
        /// </summary>
        /// <param name="path">A string representing the path. If this is not null or an empty string,
        /// it is used to provide the dialog with a default selection.</param>
        /// <param name="text">A string represeting the text to display in the dialog.</param>
        /// <returns>true if a path was selected, otherwise, false.</returns>
        public static bool Browse(ref string path, string text)
        {
            return Browse(ref path, text, false);
        }

        /// <summary>
        /// Displays a <see cref="System.Windows.Forms.FolderBrowserDialog"/> to the user.
        /// No path validation is performed.
        /// </summary>
        /// <param name="path">A string representing the path. If this is not null or an empty string,
        /// it is used to provide the dialog with a default selection.</param>
        /// <param name="text">A string represeting the text to display in the dialog.</param>
        /// <param name="showNew">A value indicating whether the New Folder button appears in the dialog.</param>
        /// <returns>true if a path was selected, otherwise, false.</returns>
        public static bool Browse(ref string path, string text, bool showNew)
        {
            using (System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.Description = text;
                dialog.ShowNewFolderButton = showNew;
                if (!string.IsNullOrEmpty(path))
                    dialog.SelectedPath = path;
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    path = dialog.SelectedPath;
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks the path for the existance of a named file.
        /// </summary>
        /// <param name="path">A string representing a path.</param>
        /// <param name="fileName">A string represeting the file to verify.</param>
        /// <returns>true if the path is valid, otherwise, false.</returns>
        public static bool IsValid(string path, string fileName)
        {
            if (string.IsNullOrEmpty(path))
                return false;
            return File.Exists(Path.Combine(path, fileName));
        }
    }
}
