﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using OmniXtract.Lib;
using Twp.Controls;
using Twp.Data.AO;

namespace OmniXtract
{
    public partial class ReportDialog : Form
    {
        #region Constructors

        ReportDialog(string header, TableLayoutPanel panel, string timer)
        {
            mHeaderLabel = new Label();
            mPanel = panel;
            mTimerLabel = new Label();
            mLine = new Line();
            mOkButton = new Button();
            SuspendLayout();
            // 
            // headerLabel
            // 
            mHeaderLabel.AutoSize = true;
            mHeaderLabel.Location = new Point(12, 9);
            mHeaderLabel.Name = "headerLabel";
            mHeaderLabel.TabIndex = 0;
            mHeaderLabel.Text = header;
            // 
            // panel
            // 
            mPanel.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            mPanel.AutoSize = true;
            mPanel.Location = new Point(12, mHeaderLabel.Bottom + 6);
            mPanel.Name = "panel";
            for (int n = 0; n < mPanel.RowCount; ++n)
                mPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            for (int n = 0; n < mPanel.ColumnCount; ++n)
                mPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            mPanel.TabIndex = 1;
            mPanel.PerformLayout();
            // 
            // timerLabel
            // 
            mTimerLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            mTimerLabel.AutoSize = true;
            mTimerLabel.Location = new Point(12, panel.Bottom + 6);
            mTimerLabel.Name = "timerLabel";
            mTimerLabel.TabIndex = 2;
            mTimerLabel.Text = timer;
            // 
            // line
            // 
            mLine.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            mLine.LinePadding = 0;
            mLine.Location = new Point(0, mTimerLabel.Bottom + 3);
            mLine.Name = "line";
            mLine.Size = new Size(mPanel.Width + 18, 2);
            // 
            // okButton
            // 
            mOkButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            mOkButton.DialogResult = DialogResult.OK;
            mOkButton.Location = new Point(mLine.Width - 84, mLine.Bottom + 9);
            mOkButton.Name = "okButton";
            mOkButton.Size = new Size(75, 23);
            mOkButton.TabIndex = 3;
            mOkButton.Text = "&Ok";
            mOkButton.UseVisualStyleBackColor = true;
            // 
            // ReportDialog
            // 
            AcceptButton = mOkButton;
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.Window;
            ClientSize = new Size(mLine.Width, mOkButton.Bottom + 9);
            Controls.Add(mHeaderLabel);
            Controls.Add(mPanel);
            Controls.Add(mTimerLabel);
            Controls.Add(mLine);
            Controls.Add(mOkButton);
            FormBorderStyle = FormBorderStyle.Sizable;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ReportDialog";
            ShowInTaskbar = false;
            SizeGripStyle =  SizeGripStyle.Hide;
            Text = "ReportDialog";
            TopMost = true;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        #region Private fields

        Button mOkButton;
        Line mLine;
        Label mHeaderLabel;
        TableLayoutPanel mPanel;
        Label mTimerLabel;

        #endregion

        #region Static methods

        static string FormatTime(TimeSpan elapsed)
        {
            string time = "Completed in ";
            if (elapsed.Hours > 0)
                time += elapsed.Hours + " hours, ";
            time += elapsed.Minutes + " minutes, ";
            time += elapsed.Seconds + " seconds.";
            return time;
        }

        public static void Show(ExtractResult result, TimeSpan elapsed)
        {
            TableLayoutPanel panel = new TableLayoutPanel();
            panel.RowCount = result.Result.Count;
            panel.ColumnCount = 2;

            int row = 0;
            foreach (var type in result.Result)
            {
                ++row;

                Label labelType = new Label();
                labelType.Size = new Size(60, 20);
                labelType.Text = ResourceType.GetName(type.Key) + "s:";
                labelType.TextAlign = ContentAlignment.MiddleRight;
                panel.Controls.Add(labelType, 0, row);

                Label labelCount = new Label();
                labelCount.Size = new Size(80, 20);
                labelCount.Text = type.Value.ToString();
                labelCount.TextAlign = ContentAlignment.MiddleCenter;
                panel.Controls.Add(labelCount, 1, row);
            }
            panel.Size = new Size(180, row * 20);

            using (ReportDialog dialog = new ReportDialog("Records extracted:", panel, FormatTime(elapsed)))
            {
                dialog.Text = "Extraction complete!";
                dialog.ShowDialog();
            }
        }

        public static void Show(ParseResult result, TimeSpan elapsed, bool compare)
        {
            TableLayoutPanel panel = new TableLayoutPanel();

            int row = compare ? 0 : -1;

            panel.RowCount = result.Result.Count + 1 + row;
            panel.ColumnCount = compare ? 4 : 2;

            if (compare)
            {
                Label labelAdded = new Label();
                labelAdded.Size = new Size(60, 20);
                labelAdded.Text = "Added";
                labelAdded.TextAlign = ContentAlignment.MiddleCenter;
                panel.Controls.Add(labelAdded, 1, 0);

                Label labelModified = new Label();
                labelModified.Size = new Size(60, 20);
                labelModified.Text = "Modified";
                labelModified.TextAlign = ContentAlignment.MiddleCenter;
                panel.Controls.Add(labelModified, 2, 0);

                Label labelRemoved = new Label();
                labelRemoved.Size = new Size(60, 20);
                labelRemoved.Text = "Removed";
                labelRemoved.TextAlign = ContentAlignment.MiddleCenter;
                panel.Controls.Add(labelRemoved, 3, 0);
            }

            foreach (var type in result.Result)
            {
                ++row;

                Label labelType = new Label();
                labelType.Size = new Size(60, 20);
                labelType.Text = ResourceType.GetName(type.Key) + "s:";
                labelType.TextAlign = ContentAlignment.MiddleRight;
                panel.Controls.Add(labelType, 0, row);

                if (compare)
                {
                    Label labelAddedCount = new Label();
                    labelAddedCount.Size = new Size(60, 20);
                    if (type.Value.ContainsKey(ItemStatus.Added))
                        labelAddedCount.Text = type.Value[ItemStatus.Added].ToString();
                    else
                        labelAddedCount.Text = "0";
                    labelAddedCount.TextAlign = ContentAlignment.MiddleCenter;
                    panel.Controls.Add(labelAddedCount, 1, row);

                    Label labelModifiedCount = new Label();
                    labelModifiedCount.Size = new Size(60, 20);
                    if (type.Value.ContainsKey(ItemStatus.Modified))
                        labelModifiedCount.Text = type.Value[ItemStatus.Modified].ToString();
                    else
                        labelModifiedCount.Text = "0";
                    labelModifiedCount.TextAlign = ContentAlignment.MiddleCenter;
                    panel.Controls.Add(labelModifiedCount, 2, row);

                    Label labelRemovedCount = new Label();
                    labelRemovedCount.Size = new Size(60, 20);
                    if (type.Value.ContainsKey(ItemStatus.Removed))
                        labelRemovedCount.Text = type.Value[ItemStatus.Removed].ToString();
                    else
                        labelRemovedCount.Text = "0";
                    labelRemovedCount.TextAlign = ContentAlignment.MiddleCenter;
                    panel.Controls.Add(labelRemovedCount, 3, row);
                }
                else
                {
                    Label labelCount = new Label();
                    labelCount.Size = new Size(60, 20);
                    if (type.Value.ContainsKey(ItemStatus.None))
                        labelCount.Text = type.Value[ItemStatus.None].ToString();
                    else
                        labelCount.Text = "0";
                    labelCount.TextAlign = ContentAlignment.MiddleCenter;
                    panel.Controls.Add(labelCount, 1, row);
                }
            }
            panel.Size = new Size(compare ? 280 : 180, ++row * 22);

            using (ReportDialog dialog = new ReportDialog("Records parsed:", panel, FormatTime(elapsed)))
            {
                dialog.Text = "Parsing complete!";
                dialog.ShowDialog();
            }
        }

        #endregion
    }
}
