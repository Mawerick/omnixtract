﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using Twp.Data.AO;

namespace OmniXtract
{
    public class DataFileInfo : IComparable<DataFileInfo>, IComparable
    {
        public DataFileInfo(FileInfo file, bool includeName)
        {
            using (DataFile dataFile = new DataFile(file.FullName, FileMode.Open, System.IO.Compression.CompressionMode.Decompress))
            {
                DataHeader header = dataFile.ReadHeader();
                if (header.Marker != Program.Marker ||
                    header.FileTag != Program.FileTag ||
                    header.FileVersion != Program.FileVersion)
                    return;
                mVersion = AOVersion.Parse(header.DataVersion);
            }

            mFile = file;
            mText = mVersion.Text;

            if (includeName)
            {
                int index = file.Name.IndexOf(Text);
                string name = file.Name.Substring(0, index);
                mText += " (" + name.Trim('_') + ")";
            }
        }

        FileInfo mFile;
        AOVersion mVersion;
        string mText;

        public FileInfo File
        {
            get { return mFile; }
        }

        public AOVersion Version
        {
            get { return mVersion; }
        }

        public string Text
        {
            get { return mText; }
        }

        public override string ToString()
        {
            return mText;
        }

        public static bool IsValid(string fileName)
        {
            using (DataFile dataFile = new DataFile(fileName, FileMode.Open, System.IO.Compression.CompressionMode.Decompress))
            {
                DataHeader header = dataFile.ReadHeader();
                if (header.Marker != Program.Marker ||
                    header.FileTag != Program.FileTag ||
                    header.FileVersion != Program.FileVersion)
                    return false;
            }
            return true;
        }

        #region IComparable

        public int CompareTo(DataFileInfo other)
        {
            if (other == null)
                return 1;
            return Version.CompareTo(other.Version);
        }

        public int CompareTo(object obj)
        {
            return CompareTo(obj as DataFileInfo);
        }

        public static bool operator <(DataFileInfo dfi1, DataFileInfo dfi2)
        {
            return dfi1.CompareTo(dfi2) < 0;
        }

        public static bool operator >(DataFileInfo dfi1, DataFileInfo dfi2)
        {
            return dfi1.CompareTo(dfi2) > 0;
        }

        #endregion
    }
}
