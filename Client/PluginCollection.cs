﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using OmniXtract.Lib;
using System;
using System.Collections;

namespace OmniXtract
{
    public class PluginCollection : CollectionBase
    {
        public void Add(PluginItem plugin)
        {
            List.Add(plugin);
        }

        public void Remove(PluginItem plugin)
        {
            List.Remove(plugin);
        }

        public PluginItem this[int index]
        {
            get { return (PluginItem)List[index]; }
        }

        public PluginItem this[string name]
        {
            get
            {
                if (string.IsNullOrEmpty(name))
                    return null;

                PluginItem item = null;

                foreach (PluginItem pi in List)
                {
                    if (pi.RealName.Equals(name))
                    {
                        item = pi;
                        break;
                    }
                }

                return item;
            }
        }

        public void Dispose()
        {
            foreach (PluginItem plugin in List)
            {
                if (plugin.Plugin is IDisposable)
                    (plugin.Plugin as IDisposable).Dispose();
            }
            Clear();
        }
    }

    public class PluginItem
    {
        public PluginItem(IPlugin plugin)
        {
            mPlugin = plugin;
        }

        IPlugin mPlugin;
        string mBaseName;
        string mRealName;
        string mName;
        string mVersion;

        string mDescription;

        public IPlugin Plugin
        {
            get { return mPlugin; }
        }

        public string BaseName
        {
            get { return mBaseName; }
            set { mBaseName = value; }
        }

        public string RealName
        {
            get { return mRealName; }
            set { mRealName = value; }
        }

        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        public string FullName
        {
            get { return mBaseName + "_" + mRealName; }
        }

        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        public string Version
        {
            get { return mVersion; }
            set { mVersion = value; }
        }
    }
}
