﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using OmniXtract.Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Twp.Data.AO;
using Twp.Utilities;

namespace OmniXtract
{
    public class Parser
    {
        IPlugin mParser;
        BackgroundWorker mWorker;
        DataFileInfo mSource;
        Dictionary<string, DataRecord> mCompare;
        bool mAborted = false;
        int mProgress = 0;

        public bool Aborted
        {
            get
            {
                if (mWorker.CancellationPending)
                    mAborted = true;
                return mAborted;
            }
        }

        public int Progress
        {
            get { return mProgress; }
            internal set
            {
                if (value >= 0 && value <= 100)
                    mProgress = value;
                OnProgressChanged(EventArgs.Empty);
            }
        }

        public List<FailedRecord> Failed { get; private set; }
        public ParseResult Result { get; private set; }

        public event MessageEventHandler Message;
        public event EventHandler ProgressChanged;

        protected virtual void OnMessage(MessageEventArgs e)
        {
            Message?.Invoke(this, e);
        }

        protected virtual void OnProgressChanged(EventArgs e)
        {
            ProgressChanged?.Invoke(this, e);
        }

        public void Initialize(PluginItem plugin, BackgroundWorker worker, DataFileInfo source, DataFileInfo compare)
        {
            if (!Directory.Exists(Program.Settings.ParsePath))
                throw new Exception("Invalid Output Path.");

            mParser = plugin?.Plugin ?? throw new Exception("No plugin selected.");
            mSource = source ?? throw new Exception("No source file selected.");
            mWorker = worker;
            Failed = new List<FailedRecord>();
            Result = new ParseResult();
            mCompare = null;

            try
            {
                string fileName = plugin.FullName + "_";
                if (compare != null)
                {
                    ReadCRCs(compare);
                    fileName += compare.Version.ToString() + "_";
                }
                fileName += source.Version.ToString();

                OnMessage(new MessageEventArgs("Initializing plugin..."));
                mParser.Initialize(Path.Combine(Program.Settings.ParsePath, fileName), source.Version);
            }
            catch (Exception ex)
            {
                throw new Exception("Plugin initialization failed. " + ex.Message, ex);
            }
        }

        void ReadCRCs(DataFileInfo compare)
        {
            OnMessage(new MessageEventArgs("Reading checksums..."));
            var dataFile = new DataFile(compare.File.FullName, FileMode.Open, System.IO.Compression.CompressionMode.Decompress);
            dataFile.ReadHeader();
            mCompare = new Dictionary<string, DataRecord>();
            while (true)
            {
                var record = dataFile.ReadRecord();
                if (record.Marker != Program.Marker)
                    throw new Exception(compare.File.Name + " is corrupt!");

                if (record.Type == 0)
                    break;

                mCompare.Add(record.Key, record);
                Progress = (int)(((double)dataFile.Position / dataFile.Length) * 100.0);
                Application.DoEvents();
            }
            dataFile.Close();
        }

        public void Parse()
        {
            mAborted = false;
            var dataFile = new DataFile(mSource.File.FullName, FileMode.Open, System.IO.Compression.CompressionMode.Decompress);
            dataFile.ReadHeader();

            OnMessage(new MessageEventArgs("Parsing..."));
            while (true)
            {
                DataRecord record = dataFile.ReadRecord();
                if (record.Marker != Program.Marker)
                    throw new Exception(mSource.File.Name + " is corrupt!");

                if (record.Type == 0)
                    break;

                ItemStatus status = ItemStatus.Added;
                if (mCompare == null)
                {
                    status = ItemStatus.None;
                }
                else
                {
                    if (mCompare.ContainsKey(record.Key))
                    {
                        if (mCompare[record.Key].CRC == record.CRC)
                            status = ItemStatus.None;
                        else
                            status = ItemStatus.Modified;

                        mCompare.Remove(record.Key);
                    }
                }
                if (mCompare == null || status != ItemStatus.None)
                {
                    ParseRecord(record.Type, record.ID, record.Data, status);
                    Result.Add(record.Type, status);
                }
                Progress = (int)(((double)dataFile.Position / dataFile.Length) * 100.0);
                Application.DoEvents();
                if (Aborted)
                {
                    dataFile.Close();
                    return;
                }
            }

            dataFile.Close();

            if (mCompare?.Count > 0)
            {
                double i = 0;
                foreach (var record in mCompare.Values)
                {
                    ParseRecord(record.Type, record.ID, record.Data, ItemStatus.Removed);
                    Result.Add(record.Type, ItemStatus.Removed);
                    Progress = (int)((i++ / mCompare.Count) * 100.0);
                    Application.DoEvents();
                    if (Aborted)
                        return;
                }
                mCompare.Clear();
            }
        }

        void ParseRecord(int type, int id, byte[] data, ItemStatus status)
        {
            try
            {
                mParser.ProcessRecord(type, id, data, status);
            }
            catch (ItemParseException ex)
            {
                Failed.Add(new FailedRecord(type, id, data, ex.Offset, ex.Trace));
                Log.Debug("[Parser.ParseRecord] Error: ", ex.Message);
                Log.Debug(ex.ToString());
            }
            catch (Exception ex)
            {
                Debugger.Break();
                Failed.Add(new FailedRecord(type, id, data, null));
                Log.Debug("[Parser.ParseRecord] Error: ", ex.Message);
                Log.Debug(ex.ToString());
            }
        }

        public void Abort()
        {
            Progress = 0;
            Application.DoEvents();
            mParser.Abort();
        }

        public void Finish()
        {
            Progress = 100;
            Application.DoEvents();
            mParser.Cleanup();
        }
    }

    public struct FailedRecord
    {
        public FailedRecord(int type, int id, byte[] data, List<ItemParseTrace> trace)
        {
            Type = type;
            Id = id;
            Data = data;
            Offset = 0;
            Trace = trace;
        }

        public FailedRecord(int type, int id, byte[] data, int offset, List<ItemParseTrace> trace)
        {
            Type = type;
            Id = id;
            Data = data;
            Offset = offset;
            Trace = trace;
        }

        public FailedRecord(string name, byte[] data, List<ItemParseTrace> trace)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            var parts = name.Split(' ');
            if (parts.Length != 2 || parts[1][0] != '#')
                throw new ArgumentException("Parameter 'name' has incorrect format.");

            Type = ResourceType.Parse(parts[0]);
            string id = parts[1].Substring(1, parts[1].IndexOf('.') - 1);
            Id = Convert.ToInt32(id);
            Data = data;
            Offset = 0;
            Trace = trace;
        }

        public int Type { get; }
        public int Id { get; }
        public byte[] Data { get; }
        public int Offset { get; }
        public List<ItemParseTrace> Trace { get; }

        public override string ToString()
        {
            return ResourceType.GetName(Type) + " #" + Id;
        }
    }

    public class ParseResult
    {
        public ParseResult()
        {
            Result = new Dictionary<int, SortedDictionary<ItemStatus, int>>();
        }

        public Dictionary<int, SortedDictionary<ItemStatus, int>> Result { get; }

        public void Add(int type, ItemStatus status)
        {
            if (Result.ContainsKey(type))
            {
                if (Result[type].ContainsKey(status))
                    Result[type][status]++;
                else
                    Result[type].Add(status, 1);
            }
            else
            {
                var record = new SortedDictionary<ItemStatus, int> { { status, 1 } };
                Result.Add(type, record);
            }
        }

        public override string ToString()
        {
            string text = "";
            foreach (var type in Result)
            {
                text += ResourceType.GetName(type.Key) + "s:";
                foreach (var record in type.Value)
                {
                    text += string.Format("\t{0}: {1,6}", record.Key, record.Value);
                }
                text += "\n";
            }
            return text;
        }
    }
}
