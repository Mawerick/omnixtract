﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;
using Twp.Controls;

namespace OmniXtract
{
    [ToolboxBitmap(typeof(System.Windows.Forms.TabControl))]
    public class StyledTabControl : System.Windows.Forms.TabControl
    {
        #region Constructor

        public StyledTabControl()
        {
            SetStyle(ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.Opaque |
                ControlStyles.ResizeRedraw, true);
        }

        #endregion

        protected override void OnSelecting(TabControlCancelEventArgs e)
        {
            base.OnSelecting(e);
            e.Cancel = !e.TabPage.Enabled;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Color color = BackColor;
            if (Parent != null)
                color = Parent.BackColor;
            using (Brush brush = new SolidBrush(color))
                e.Graphics.FillRectangle(brush, ClientRectangle);

            PaintPage(e.Graphics);
            for (int i = 0; i < TabCount; ++i)
                PaintTab(e.Graphics, i, i == SelectedIndex);
        }

        Color GetPageColor(int index)
        {
            Color color = TabPages[index].BackColor;
            if (TabPages[index].UseVisualStyleBackColor)
                color = SystemColors.Window;
            if (!TabPages[index].Enabled)
                color = Utilities.AdjustColor(color, -0.05f);
            return color;
        }

        Brush GetTabBrush(Rectangle rect, Color color)
        {
            switch (Alignment)
            {
                case TabAlignment.Top:
                    return GradientBrush.Create(rect, SystemColors.Highlight, color, GradientMode.Vertical);
                case TabAlignment.Bottom:
                    return GradientBrush.Create(rect, color, SystemColors.Highlight, GradientMode.Vertical);
                case TabAlignment.Left:
                    return GradientBrush.Create(rect, SystemColors.Highlight, color, GradientMode.Horizontal);
                case TabAlignment.Right:
                    return GradientBrush.Create(rect, color, SystemColors.Highlight, GradientMode.Horizontal);
                default:
                    throw new Exception("Unknown Alignment " + Alignment);
            }
        }

        void PaintTab(Graphics g, int index, bool selected)
        {
            string text = TabPages[index].Text;
            Rectangle rect = GetTabRect(index);
            Color backColor = GetPageColor(index);
            Point[] border = null;
            Point[] selectEdge = null;

            switch (Alignment)
            {
                case TabAlignment.Top:
                    border = new Point[]
                    {
                        new Point(rect.Left, rect.Bottom),
                        new Point(rect.Left, rect.Top),
                        new Point(rect.Right, rect.Top),
                        new Point(rect.Right, rect.Bottom),
                    };
                    break;
                case TabAlignment.Bottom:
                    border = new Point[]
                    {
                        new Point(rect.Left, rect.Top),
                        new Point(rect.Left, rect.Bottom),
                        new Point(rect.Right, rect.Bottom),
                        new Point(rect.Right, rect.Top),
                    };
                    break;
                case TabAlignment.Left:
                    border = new Point[]
                    {
                        new Point(rect.Right, rect.Bottom),
                        new Point(rect.Left, rect.Bottom),
                        new Point(rect.Left, rect.Top),
                        new Point(rect.Right, rect.Top),
                    };
                    break;
                case TabAlignment.Right:
                    border = new Point[]
                    {
                        new Point(rect.Left, rect.Bottom),
                        new Point(rect.Right, rect.Bottom),
                        new Point(rect.Right, rect.Top),
                        new Point(rect.Left, rect.Top),
                    };
                    break;
            }

            using (Brush brush = new SolidBrush(backColor))
                g.FillRectangle(brush, rect);

            if (selected)
            {
                Brush brush = null;
                Rectangle selRect = rect;
                switch (Alignment)
                {
                    case TabAlignment.Top:
                        selRect.Height /= 3;
                        brush = GradientBrush.Create(selRect, Color.SkyBlue, backColor, GradientMode.Vertical);
                        selectEdge = new Point[]
                        {
                            new Point(rect.Left + 1, rect.Bottom),
                            new Point(rect.Right - 1, rect.Bottom),
                        };
                        break;
                    case TabAlignment.Bottom:
                        selRect.Height /= 3;
                        selRect.Y += selRect.Height * 2;
                        brush = GradientBrush.Create(selRect, backColor, Color.SkyBlue, GradientMode.Vertical);
                        selRect.Y += 1;
                        selectEdge = new Point[]
                        {
                            new Point(rect.Left + 1, rect.Top - 1),
                            new Point(rect.Right - 1, rect.Top - 1),
                        };
                        break;
                    case TabAlignment.Left:
                        selRect.Width /= 3;
                        brush = GradientBrush.Create(selRect, Color.SkyBlue, backColor, GradientMode.Horizontal);
                        selectEdge = new Point[]
                        {
                            new Point(rect.Right, rect.Top + 1),
                            new Point(rect.Right, rect.Bottom - 1),
                        };
                        break;
                    case TabAlignment.Right:
                        selRect.Width /= 3;
                        selRect.X += selRect.Width * 2;
                        brush = GradientBrush.Create(selRect, backColor, Color.SkyBlue, GradientMode.Horizontal);
                        selRect.X += 1;
                        selectEdge = new Point[]
                        {
                            new Point(rect.Left - 1, rect.Top + 1),
                            new Point(rect.Left - 1, rect.Bottom - 1),
                        };
                        break;
                }
                if (brush != null)
                {
                    g.FillRectangle(brush, selRect);
                }
            }

            g.DrawLines(SystemPens.ControlDark, border);
            if (selectEdge != null)
            {
                using (Pen pen = new Pen(backColor))
                    g.DrawLines(pen, selectEdge);
            }

            TextFormatFlags tff = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter;
            if (TabPages[index].Enabled)
                TextRenderer.DrawText(g, text, Font, rect, ForeColor, tff);
            else
                TextRenderer.DrawText(g, text, Font, rect, SystemColors.GrayText, tff);
        }

        void PaintPage(Graphics g)
        {
            Rectangle rect;
            Color backColor = BackColor;
            if (TabCount > 0)
            {
                int index = 0;
                if (SelectedIndex > 0)
                    index = SelectedIndex;
                backColor = GetPageColor(index);
                rect = TabPages[index].Bounds;
                rect.Inflate(2, 2);
            }
            else
            {
                rect = ClientRectangle;
                switch (Alignment)
                {
                    case TabAlignment.Top:
                        rect.Y += ItemSize.Height;
                        rect.Height -= ItemSize.Height;
                        break;
                    case TabAlignment.Bottom:
                        rect.Height -= ItemSize.Height;
                        break;
                    case TabAlignment.Left:
                        rect.X += ItemSize.Width;
                        rect.Width -= ItemSize.Width;
                        break;
                    case TabAlignment.Right:
                        rect.Width -= ItemSize.Width;
                        break;
                }
            }
            rect.Width -= 1;
            rect.Height -= 1;

            using (Brush brush = new SolidBrush(backColor))
                g.FillRectangle(brush, rect);
            g.DrawRectangle(SystemPens.ControlDark, rect);
        }
    }
}
