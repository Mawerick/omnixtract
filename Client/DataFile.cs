﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.IO.Compression;

namespace OmniXtract
{
    public sealed class DataFile : IDisposable
    {
        #region Constructor

        public DataFile(string fileName, FileMode fileMode, CompressionMode compressionMode)
        {
            mStream = new FileStream(fileName, fileMode, FileAccess.ReadWrite, FileShare.Read);
            mReader = new BinaryReader(mStream);
            mWriter = new BinaryWriter(mStream);
            mZipStream = new GZipStream(mStream, compressionMode);
        }

        #endregion

        #region Private fields

        FileStream mStream;
        BinaryReader mReader;
        BinaryWriter mWriter;
        GZipStream mZipStream;

        #endregion

        #region Properties

        public FileStream FileStream
        {
            get { return mStream; }
        }

        public BinaryReader Reader
        {
            get { return mReader; }
        }

        public BinaryWriter Writer
        {
            get { return mWriter; }
        }

        public GZipStream ZipStream
        {
            get { return mZipStream; }
        }

        public long Position
        {
            get { return mStream.Position; }
        }

        public long Length
        {
            get { return mStream.Length; }
        }

        #endregion

        #region Close/Dispose Methods

        public void Close()
        {
            mZipStream.Close();
            mZipStream.Dispose();
            mReader.Close();
            mWriter.Close();
            mStream.Close();
            mStream.Dispose();
        }

        public void Dispose()
        {
            Close();
        }

        #endregion

        #region Write Methods

        public void Write(byte[] buffer, int offset = 0, int count = -1)
        {
            mZipStream.Write(buffer, offset, (count == -1 ? buffer.Length - offset : count));
        }

        public void Write(int value)
        {
            var buffer = BitConverter.GetBytes(value);
            mZipStream.Write(buffer, 0, buffer.Length);
        }

        public void Write(uint value)
        {
            var buffer = BitConverter.GetBytes(value);
            mZipStream.Write(buffer, 0, buffer.Length);
        }

        public void Write(ulong value)
        {
            var buffer = BitConverter.GetBytes(value);
            mZipStream.Write(buffer, 0, buffer.Length);
        }

        public void Write(DataHeader header)
        {
            Write(header.Marker);
            Write(header.FileTag);
            Write(header.FileVersion);
            Write(header.DataVersion);
        }

        public void Write(DataRecord record)
        {
            Write(record.Marker);
            Write(record.Type);
            Write(record.ID);
            Write(record.Length);
            Write(record.CRC);
            Write(record.Data);
        }

        #endregion

        #region Read Methods

        public byte[] Read(int count)
        {
            var buffer = new byte[count];
            mZipStream.Read(buffer, 0, count);
            return buffer;
        }

        public int ReadInt32()
        {
            var buffer = new byte[4];
            mZipStream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }

        public uint ReadUInt32()
        {
            var buffer = new byte[4];
            mZipStream.Read(buffer, 0, 4);
            return BitConverter.ToUInt32(buffer, 0);
        }

        public ulong ReadUInt64()
        {
            var buffer = new byte[8];
            mZipStream.Read(buffer, 0, 8);
            return BitConverter.ToUInt64(buffer, 0);
        }

        public DataHeader ReadHeader()
        {
            DataHeader header = new DataHeader
            {
                Marker = ReadUInt32(),
                FileTag = ReadUInt32(),
                FileVersion = ReadInt32(),
                DataVersion = Read(5)
            };
            return header;
        }

        public DataRecord ReadRecord()
        {
            DataRecord record = new DataRecord
            {
                Marker = ReadUInt32(),
                Type = ReadInt32(),
                ID = ReadInt32(),
                Length = ReadInt32(),
                CRC = ReadUInt64()
            };
            record.Data = Read(record.Length);
            return record;
        }

        #endregion
    }

    #region Data structs

    public struct DataHeader
    {
        public DataHeader(byte[] dataVersion)
        {
            mMarker = Program.Marker;
            mFileTag = Program.FileTag;
            mFileVersion = Program.FileVersion;
            mDataVersion = dataVersion;
        }

        uint mMarker;
        uint mFileTag;
        int mFileVersion;
        byte[] mDataVersion;

        public uint Marker
        {
            get { return mMarker; }
            set { mMarker = value; }
        }

        public uint FileTag
        {
            get { return mFileTag; }
            set { mFileTag = value; }
        }

        public int FileVersion
        {
            get { return mFileVersion; }
            set { mFileVersion = value; }
        }

        public byte[] DataVersion
        {
            get { return mDataVersion; }
            set { mDataVersion = value; }
        }
    }

    public struct DataRecord
    {
        uint mMarker;
        int mType;
        int mID;
        int mLength;
        ulong mCRC;
        byte[] mData;

        public uint Marker
        {
            get { return mMarker; }
            set { mMarker = value; }
        }

        public int Type
        {
            get { return mType; }
            set { mType = value; }
        }

        public int ID
        {
            get { return mID; }
            set { mID = value; }
        }

        public int Length
        {
            get { return mLength; }
            set { mLength = value; }
        }

        public ulong CRC
        {
            get { return mCRC; }
            set { mCRC = value; }
        }

        public byte[] Data
        {
            get { return mData; }
            set { mData = value; }
        }

        public string Key
        {
            get { return Type.ToString() + ":" + ID.ToString(); }
        }
    }

    #endregion
}
