﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using OmniXtract.Lib;
using System;
using System.IO;
using System.Reflection;
using Twp.Utilities;

namespace OmniXtract
{
    public static class PluginManager
    {
        static PluginCollection mPlugins = null;
        static PluginItem mSelected = null;
        static string mPath = null;
        static string mError = null;

        public static PluginCollection Plugins
        {
            get { return mPlugins; }
        }

        public static PluginItem Selected
        {
            get { return mSelected; }
            set { mSelected = value; }
        }

        public static string Error
        {
            get { return mError; }
        }

        public static bool Open()
        {
            return Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins"));
        }

        public static bool Open(string path)
        {
            if (mPlugins != null)
                Close();

            mPlugins = new PluginCollection();
            mPath = path;
            mError = string.Empty;
            int errors = 0;

            if (!System.IO.Directory.Exists(path))
            {
                Log.Warning("Plugin path not found: {0}", path);
                return false;
            }

            foreach (string fileName in System.IO.Directory.GetFiles(path, "*.dll"))
            {
                string baseName = System.IO.Path.GetFileName(fileName);
                try
                {
                    Assembly asm = Assembly.LoadFrom(fileName);
                    if (asm == null)
                    {
                        Log.Debug("[PluginManager.Open] Failed to load assembly from file: {0}", fileName);
                        mError += baseName + ": Unknown error." + Environment.NewLine;
                        ++errors;
                        continue;
                    }

                    foreach (Type type in asm.GetTypes())
                    {
                        if (!type.IsPublic && !type.IsClass)
                            continue;

                        Type pluginType = type.GetInterface(typeof(IPlugin).ToString(), true);
                        if (pluginType != null)
                        {
                            IPlugin plugin = (IPlugin)Activator.CreateInstance(type);
                            Log.Debug("[PluginManager.Open] Adding Plugin: {0}", plugin);

                            PluginItem item = new PluginItem(plugin);
                            string[] names = type.FullName.Split('.');
                            item.BaseName = names[0];
                            item.RealName = names[names.Length - 1];
                            item.Version = asm.GetName().Version.ToString();

                            object[] attrs = type.GetCustomAttributes(typeof(PluginNameAttribute), false);
                            if (attrs != null && attrs.Length == 1)
                                item.Name = ((PluginNameAttribute)attrs[0]).Name;
                            else
                                item.Name = item.RealName;

                            attrs = type.GetCustomAttributes(typeof(PluginDescriptionAttribute), false);
                            if (attrs != null && attrs.Length == 1)
                                item.Description = ((PluginDescriptionAttribute)attrs[0]).Text;
                            else
                                item.Description = string.Empty;

                            mPlugins.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Debug("[PluginManager.Open] Caught exception: {0} File: {1}", ex.Message, fileName);
                    mError += baseName + ": " + ex.Message + Environment.NewLine;
                    ++errors;
                    continue;
                }
            }

            if (errors > 0)
                mError = "Failed to load " + errors + " plugins: " + Environment.NewLine + Environment.NewLine + mError;

            return mPlugins.Count > 0 ? true : false;
        }

        public static void Close()
        {
            if (mPlugins != null)
                mPlugins.Dispose();
        }
    }
}
