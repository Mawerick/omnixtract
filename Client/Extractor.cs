﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;
using Twp.Controls;
using Twp.Data.AO;
using Twp.Utilities;

namespace OmniXtract
{
    public class Extractor
    {
        string mFilename;
        DataFile mDataFile;
        ResourceDatabase mRdb;
        MD5CryptoServiceProvider mD5;
        BackgroundWorker mWorker;
        ExtractResult mResult;
        bool mDecode = false;
        bool mLargeFiles = false;
        bool mUseTransaction = true;
        bool mAborted = false;

        public bool UseTransaction
        {
            get { return mUseTransaction; }
            set { mUseTransaction = value; }
        }

        public bool Aborted
        {
            get { return mAborted; }
        }

        public ExtractResult Result
        {
            get { return mResult; }
        }

        public void Initialize(PluginItem plugin, BackgroundWorker worker)
        {
            mAborted = false;
            mWorker = worker;

            if (!PathUtils.IsValid(Program.Settings.GamePath, "AnarchyOnline.exe") && !PathUtils.IsValid(Program.Settings.GamePath, "Clientr.exe"))
                throw new Exception("Invalid Game Path.");
            if (!Directory.Exists(Program.Settings.ExtractPath))
                throw new Exception("Invalid Output Path.");

            string path = Program.Settings.GamePath;
            AOVersion version = AOVersion.Read(path);
            if (version == null)
                throw new Exception("Failed to read RDB version.");

            mDecode = version < new AOVersion(13, 91, -1); // first db without decoded data
            mLargeFiles = version > new AOVersion(18, 6, 6); // last 32-bit db

            mFilename = Path.Combine(Program.Settings.ExtractPath, string.Format("{0}_{1}{2}", plugin.FullName, version.Text, Program.FileExt));
            if (File.Exists(mFilename))
            {
                DialogResult dr = MessageBox.Show("A database file already exists. Overwrite?",
                    "Overwrite?",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                switch (dr)
                {
                    case DialogResult.Yes:
                        File.Delete(mFilename);
                        break;
                    case DialogResult.No:
                        mFilename = PathExt.GetSafeName(mFilename);
                        break;
                    default:
                        throw new Exception("Aborted by user");
                }
            }
            mDataFile = new DataFile(mFilename, FileMode.Create, System.IO.Compression.CompressionMode.Compress);
            mDataFile.Write(new DataHeader(version.ToByteArray()));
            mRdb = new ResourceDatabase(Program.Settings.GamePath, mLargeFiles);
            mD5 = new MD5CryptoServiceProvider();
            mResult = new ExtractResult();
        }

        public void Finish()
        {
            DataRecord finalRecord = new DataRecord
            {
                Marker = Program.Marker,
                Data = new byte[0]
            };
            mDataFile.Write(finalRecord);

            mDataFile.Dispose();
            mRdb.Dispose();
        }

        public void Abort()
        {
            mDataFile.Dispose();
            if (File.Exists(mFilename))
                File.Delete(mFilename);
            mRdb.Dispose();
        }

        public void Extract(Node node)
        {
            int type = (int)node.Tag;

            if (!mRdb.IsValidType(type))
            {
                node.Fields[2] = "Missing";
                return;
            }

            int[] ids = mRdb.GetRecordIds(type);
            if (ids == null || ids.Length <= 0)
            {
                node.Fields[2] = "Missing";
                return;
            }

            string table = "data_" + type;

            node.EnsureVisible();
            if (node.Fields[2] == null || !(node.Fields[2] is ProgressBar))
                node.Fields[2] = new ProgressBar();
            ProgressBar progressBar = node.Fields[2] as ProgressBar;
            progressBar.Value = 0;
            int count = 0;
            int interval = ids.Length / 100;
            int n = 0;
            foreach (int id in ids)
            {
                if (n >= interval)
                    n = 0;
                try
                {
                    // Extract and process data...
                    byte[] data = mRdb.GetRecordData(type, id, mDecode);

                    // Compute data hash, used for comparison...
                    byte[] hash = mD5.ComputeHash(data, 0, data.Length);
                    ulong crca = BitConverter.ToUInt64(hash, 0);
                    ulong crcb = BitConverter.ToUInt64(hash, 8);

                    DataRecord record = new DataRecord
                    {
                        Marker = Program.Marker,
                        Type = type,
                        ID = id,
                        Length = data.Length,
                        Data = data,
                        CRC = crca ^ crcb
                    };
                    mDataFile.Write(record);

                    Application.DoEvents();
                    if (mAborted || mWorker.CancellationPending)
                        return;
                }
                catch (InvalidDataException ex)
                {
                    Log.Debug("[MainForm.OnExtract] Extract failed in 0x{0:X2}::{1}: {2}", type, id, ex.Message);
                }
                catch (Exception ex)
                {
                    Log.Error("Plugin processing failed. " + ex.Message);
                    mAborted = true;
                    return;
                }
                ++count;
                if (n == 0)
                {
                    if (progressBar.Value < progressBar.Maximum)
                        ++progressBar.Value;
                    node.Fields[3] = string.Format("{0}/{1}", count, ids.Length);
                    node.Refresh();
                }
                ++n;
            }
            progressBar.Dispose();
            progressBar = null;
            node.Fields[2] = "Done";
            node.Fields[3] = count;
            node.Refresh();
            mResult.Add(type, count);
            Application.DoEvents();
        }
    }

    public class ExtractResult
    {
        public ExtractResult()
        {
            mResult = new Dictionary<int, int>();
        }

        Dictionary<int, int> mResult;

        public Dictionary<int, int> Result
        {
            get { return mResult; }
        }

        public void Add(int type, int count)
        {
            if (mResult.ContainsKey(type))
                mResult[type] = count;
            else
                mResult.Add(type, count);
        }

        public override string ToString()
        {
            string text = "";
            foreach (var type in mResult)
            {
                text += string.Format("{0}s:\t{1}\n", ResourceType.GetName(type.Key), type.Value);
            }
            return text;
        }
    }
}
