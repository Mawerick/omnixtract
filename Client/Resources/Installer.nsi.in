;
;  Copyright (C) Mawerick, WrongPlace.Net 2015
;
;  This program is free software: you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

;---------------------------------------
;include Modern UI

  !include "MUI2.nsh"
  
;---------------------------------------
;defined constants

  !define UNINST_REGKEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\@PRODUCT@"

;---------------------------------------
;General

  ;Name and file
  Name "@PRODUCT@"
  OutFile "@CWD@\\..\\..\\_Releases\\@PRODUCT@\\@PRODUCT@-@VERSION_SHORT@.exe"
  BrandingText "@COMPANY@ | @URL@"

  ;Default install folder
  InstallDir $PROGRAMFILES\@COMPANY@\@PRODUCT@

  ;Get install folder from registry if available
  InstallDirRegKey HKLM "Software\@COMPANY@\@PRODUCT@" "InstallPath"

  ;Request app priveleges (Vista)
  RequestExecutionLevel admin

  SetCompressor lzma

;---------------------------------------
;Variables

  Var StartMenuFolder

;--------------------------------
;Version Information

  VIProductVersion "@VERSION_SHORT@.0"
  VIAddVersionKey "ProductName" "@PRODUCT@"
  VIAddVersionKey "Comments" "@DESCRIPTION@"
  VIAddVersionKey "CompanyName" "@COMPANY@"
  VIAddVersionKey "LegalCopyright" "@COPYRIGHT@"
  VIAddVersionKey "FileDescription" "@PRODUCT@ Installer"
  VIAddVersionKey "FileVersion" "@VERSION_SHORT@.0"

;---------------------------------------
;Interface settings

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "installer.bmp"
  !define MUI_WELCOMEFINISHPAGE_BITMAP "installer_wf.bmp"
  !define MUI_ABORTWARNING
  !define MUI_FINISHPAGE_NOAUTOCLOSE
  !define MUI_UNFINISHPAGE_NOAUTOCLOSE

;---------------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "@LICENSE@"
  !insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\@COMPANY@\@PRODUCT@"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  !define MUI_STARTMENUPAGE_DEFAULTFOLDER "@COMPANY@\@PRODUCT@"
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

  !insertmacro MUI_PAGE_INSTFILES

  ;Finish Page Configuration
  ;!define MUI_FINISHPAGE_RUN $INSTDIR\@PRODUCT@.exe
  ;!define MUI_FINISHPAGE_RUN_TEXT "Run @PRODUCT@ now."
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;---------------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;---------------------------------------
;Functions
Function RemoveOldInstall
  ReadRegStr $0 HKLM "SOFTWARE\@COMPANY@\@PRODUCT@" "InstallPath"

  IfFileExists "$0\uninstall.exe" 0 +2
    ExecWait '"$0\uninstall.exe" /S _?=$0'

  ReadRegStr $0 HKLM "SOFTWARE\@PRODUCT@" "InstallPath"

  IfFileExists "$0\uninstall.exe" 0 +2
    ExecWait '"$0\uninstall.exe" /S _?=$0'

FunctionEnd

;---------------------------------------
;Installer sections
Section "" SecInstall

  SectionIn RO

  Call RemoveOldInstall

  SetOutPath $INSTDIR\x86
  @FILES_x86@

  SetOutPath $INSTDIR\x64
  @FILES_x64@

  SetOutPath $INSTDIR\Plugins
  @FILES_PLUGINS@

  SetOutPath $INSTDIR\Config
  @FILES_CONFIG@

  SetOutPath $INSTDIR
  @FILES@

  ;Store install folder
  WriteRegStr HKLM "SOFTWARE\@COMPANY@\@PRODUCT@" "InstallPath" "$INSTDIR"

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\uninstall.exe"
  WriteRegStr HKLM "${UNINST_REGKEY}" "DisplayName" "$(^Name)"
  WriteRegStr HKLM "${UNINST_REGKEY}" "DisplayIcon" "$INSTDIR\@PRODUCT@.exe"
  WriteRegStr HKLM "${UNINST_REGKEY}" "DisplayVersion" "@VERSION@"
  WriteRegStr HKLM "${UNINST_REGKEY}" "UninstallString" "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "${UNINST_REGKEY}" "URLInfoAbout" "@URL@"
  WriteRegStr HKLM "${UNINST_REGKEY}" "Publisher" "@COMPANY@"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application

    ;Create shortcuts
    SetShellVarContext all
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    @SHORTCUTS@
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

;---------------------------------------
;Uninstaller section
Section "Uninstall"

  ;Delete files and folders
  Delete "$INSTDIR\x86\*.*"
  RMDir "$INSTDIR\x86"
  Delete "$INSTDIR\x64\*.*"
  RMDir "$INSTDIR\x64"
  Delete "$INSTDIR\Plugins\*.*"
  RMDir "$INSTDIR\Plugins"
  Delete "$INSTDIR\Config\*.*"
  RMDir "$INSTDIR\Config"
  Delete "$INSTDIR\*.*"
  RMDir "$INSTDIR"

  ;Delete startmenu folder
  SetShellVarContext all
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  Delete "$SMPROGRAMS\$StartMenuFolder\*.*"
  RMDir "$SMPROGRAMS\$StartMenuFolder"

  DeleteRegKey HKLM "Software\@COMPANY@\@PRODUCT@"
  DeleteRegKey /ifempty HKLM "Software\@COMPANY@"
  DeleteRegKey HKLM "${UNINST_REGKEY}"

SectionEnd
