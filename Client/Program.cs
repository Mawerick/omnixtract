﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using Twp.Utilities;

namespace OmniXtract
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
#if !DEBUG
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(OnThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
#endif
            Log.DebugOutput = Log.OutputType.Trace;
            Log.Start("OmniXtract.log", PathExt.GetAppDataPath());
            Settings.Load();

            Application.Run(new MainForm());

            Settings.Save();
        }

        internal static Settings Settings = new Settings();

        public const uint Marker = 0x4F584442;
        public const uint FileTag = 0x4D617721;
        public const int FileVersion = 0x010000;
        public const string FileExt = ".oxdb";

#if !DEBUG
        private static void OnThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Twp.Controls.ExceptionDialog.Show(e.Exception);
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Twp.Controls.ExceptionDialog.Show(e.ExceptionObject as Exception);
        }
#endif
    }
}
