//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using Twp.Controls;
using Twp.Utilities;

namespace OmniXtract
{
    public class Settings
    {
        public Settings()
        {
            mDoc = new IniDocument("OmniXtract.ini", PathExt.GetAppDataPath());
        }

        IniDocument mDoc;

        public string GamePath
        {
            get { return mDoc["General"]["GamePath"]; }
            set { mDoc["General"]["GamePath"] = value; }
        }

        public string ExtractPath
        {
            get { return mDoc["General"]["ExtractPath"]; }
            set { mDoc["General"]["ExtractPath"] = value; }
        }

        public string ParsePath
        {
            get { return mDoc["General"]["ParsePath"]; }
            set { mDoc["General"]["ParsePath"] = value; }
        }

        public string Plugin
        {
            get { return mDoc["General"]["Plugin"]; }
            set { mDoc["General"]["Plugin"] = value; }
        }

        public bool UseTransactions
        {
            get { return Convert.ToBoolean(mDoc["Advanced"]["UseTransactions"]); }
            set { mDoc["Advanced"]["UseTransactions"] = Convert.ToString(value); }
        }

        public void Load()
        {
            try
            {
                mDoc.Read();
            }
            catch (IniParseException ex)
            {
                Log.Warning(ex.Message);
                Log.Debug("[Settings.Load] {0}", ex.Line);
            }
            mDoc.SetDefault("General", "GamePath", string.Empty);
            mDoc.SetDefault("General", "ExtractPath", string.Empty);
            mDoc.SetDefault("General", "ParsePath", string.Empty);
            mDoc.SetDefault("General", "Plugin", string.Empty);
            mDoc.SetDefault("Advanced", "UseTransactions", "true");
        }

        public void Save()
        {
            try
            {
                mDoc.Write();
            }
            catch (Exception ex)
            {
                ExceptionDialog.Show(ex);
            }
        }
    }
}
