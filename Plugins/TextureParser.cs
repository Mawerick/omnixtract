﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using OmniXtract.Lib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Twp.Data.AO;
using Twp.Utilities;

namespace OmniXtract.Plugins
{
    [PluginName("Texture Extractor")]
    public class TexturesExtractor : IPlugin
    {
        public int[] Types
        {
            get
            {
                return new int[]
                {
                    ResourceType.Icon,
                    ResourceType.Textures_High,
                    //ResourceType.Textures_Med,
                    //ResourceType.Textures_Low,
                    //ResourceType.Textures_ACG_High,
                    //ResourceType.Textures_ACG_Med,
                    //ResourceType.Textures_ACG_Low,
                    //ResourceType.Textures_Skin_High,
                    //ResourceType.Textures_Skin_Med,
                    //ResourceType.Textures_Skin_Low
                };
            }
        }

        string mOutputPath;
        Dictionary<int, string> mPaths = new Dictionary<int, string>();

        public void Initialize(string output, AOVersion version)
        {
            mOutputPath = output;
            mPaths.Clear();
        }

        public void Cleanup()
        {
            foreach (string path in mPaths.Values)
                DeleteIfEmpty(path);
            DeleteIfEmpty(mOutputPath);

            mPaths.Clear();
        }

        public void Abort()
        {
        }

        public void BeginType(int type)
        {
            string name;
            switch (type)
            {
                case ResourceType.Icon:
                    name = "Icons";
                    break;
                case ResourceType.Textures_High:
                    name = "Textures_High";
                    break;
                case ResourceType.Textures_Med:
                    name = "Textures_Medium";
                    break;
                case ResourceType.Textures_Low:
                    name = "Textures_Low";
                    break;
                case ResourceType.Textures_ACG_High:
                    name = "Textures_ACG_High";
                    break;
                case ResourceType.Textures_ACG_Med:
                    name = "Textures_ACG_Medium";
                    break;
                case ResourceType.Textures_ACG_Low:
                    name = "Textures_ACG_Low";
                    break;
                case ResourceType.Textures_Skin_High:
                    name = "CharSkin_High";
                    break;
                case ResourceType.Textures_Skin_Med:
                    name = "CharSkin_Medium";
                    break;
                case ResourceType.Textures_Skin_Low:
                    name = "CharSkin_Low";
                    break;

                default:
                    return;
            }
            mPaths.Add(type, Path.Combine(mOutputPath, name));
        }

        public void ProcessRecord(int type, int id, byte[] data, ItemStatus status)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    Bitmap image = (Bitmap)Image.FromStream(ms);

                    if (!mPaths.ContainsKey(type))
                        BeginType(type);
                    string path = mPaths[type];
                    if (string.IsNullOrEmpty(path))
                        throw new Exception("Type path not set! Did we not run BeginExtract?");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    string prefix = "";
                    if (status != ItemStatus.None)
                        prefix = status.ToString() + "_";
                    string filename = System.IO.Path.Combine(path, prefix + id.ToString() + ".png");
                    if (File.Exists(filename))
                        File.Delete(filename);

                    //image.MakeTransparent(Color.Lime);
                    Twp.Data.AO.Icon.MakeTransparent(ref image);
                    image.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            catch (ArgumentException ex)
            {
                Log.Debug("[TexturesPlugin.ProcessRecord] Not an image: 0x{0:X2}::{1}: {2}", type, id, ex);
            }
        }

        void DeleteIfEmpty(string path)
        {
            if (string.IsNullOrEmpty(path) || !Directory.Exists(path))
                return;

            if (Directory.GetFileSystemEntries(path).Length > 0)
                return;

            Directory.Delete(path);
        }
    }
}
