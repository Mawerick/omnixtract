﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using OmniXtract.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Twp.Data.AO;
using Twp.Utilities;

namespace OmniXtract.Plugins
{
    [PluginName("XML Item Parser")]
    public class XMLParser : IPlugin
    {
        public int[] Types
        {
            get
            {
                return new int[]
                {
                    ResourceType.Item,
                    ResourceType.Nano,
                    ResourceType.Perk
                };
            }
        }

        XmlDocument mDoc;
        Dictionary<int, XmlElement> mTypeElements;
        XmlElement mTypeElement;
        string mFilename;
        Formatter mFormatter;
        FunctionSets mSets;

        public void Initialize(string output, AOVersion version)
        {
            mFilename = output + ".xml";
            if (File.Exists(mFilename))
            {
            }

            mDoc = new XmlDocument();
            mDoc.AppendChild(mDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes"));
            mDoc.AppendChild(mDoc.CreateProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"omnixtract.xsl\""));
            mDoc.AppendChild(mDoc.CreateElement("Root"));

            mFormatter = new Formatter();
            mSets = new FunctionSets("FunctionSets.ini", Path.Combine(PathExt.GetExecPath(), "Config"), version);
            mTypeElements = new Dictionary<int, XmlElement>();

            ItemParser.Trace = true;
        }

        public void Abort()
        {
            ItemParser.Trace = false;
        }

        public void Cleanup()
        {
            foreach(XmlElement element in mTypeElements.Values)
            {
                if (element.HasChildNodes)
                    mDoc.DocumentElement.AppendChild(element);
            }
            mDoc.Save(mFilename);
            ItemParser.Trace = false;
        }
        
        public void ProcessRecord(int type, int id, byte[] data, ItemStatus status)
        {
            if (mTypeElements.ContainsKey(type))
                mTypeElement = mTypeElements[type];
            else
            {
                mTypeElement = mDoc.CreateElement("Type");
                mTypeElement.SetAttribute("ID", type.ToString());
                mTypeElement.SetAttribute("Name", ResourceType.GetName(type));
                mTypeElements.Add(type, mTypeElement);
            }
            if (type == ResourceType.Item)
            {
                ItemInfo info = ItemParser.Parse(id, data, mSets);
                ProcessItem(info, status);
            }
            else if (type == ResourceType.Nano)
            {
                ItemInfo info = ItemParser.Parse(id, data, mSets);
                info.Type = ItemType.Nano;
                ProcessItem(info, status);
            }
            else if (type == ResourceType.Perk)
            {
                ProcessPerk(new Perk(id, data), status);
            }
            mTypeElement = null;
        }

        void ProcessItem(ItemInfo info, ItemStatus status)
        {
            if (info == null || mTypeElement == null)
                return;

            string name = "Item";
            if (info.Type == ItemType.Nano)
                name = "Nano";

            XmlElement element = mDoc.CreateElement(name);
            element.SetAttribute("ID", info.Id.ToString());
            element.SetAttribute("Name", info.Name);
            element.SetAttribute("QL", info.QL.ToString());
            element.SetAttribute("Status", status.ToString());

            if (!string.IsNullOrEmpty(info.Description))
            {
                XmlElement description = mDoc.CreateElement("Description");
                description.InnerXml = Text.HtmlToXml(info.Description);
                element.AppendChild(description);
            }

            ProcessAttributes(info, element);
            ProcessAtkDef(info, element);
            ProcessActions(info, element);
            ProcessEvents(info, element);

            mTypeElement.AppendChild(element);
        }

        void ProcessAttributes(ItemInfo info, XmlElement parent)
        {
            XmlElement element = mDoc.CreateElement("Attributes");
            foreach (var attr in info.Attributes)
            {
                string attrName = mFormatter.AttrName(attr.Key);
                if (info.Type == ItemType.Nano)
                {
                    switch (attrName.ToUpper())
                    {
                        case "CLANTOKENS": attrName = "Alignment"; break;
                        case "OMNITOKENS": attrName = "Strain"; break;
                        case "LEVEL": attrName = "NCUCost"; break;
                    }
                }
                else
                {
                    if (attrName.ToUpper() == "LEVEL")
                        attrName = "QL";
                }
                string attrVal = mFormatter.AttrValue(attr.Key, attr.Value, (int)info.Type);
                if (string.IsNullOrEmpty(attrVal))
                    attrVal = "0";
                XmlElement attrElement = mDoc.CreateElement("Attribute");
                attrElement.SetAttribute("Name", attrName);
                attrElement.SetAttribute("Value", attrVal);
                element.AppendChild(attrElement);
            }
            if (element.HasChildNodes)
                parent.AppendChild(element);
        }

        void ProcessAtkDef(ItemInfo info, XmlElement parent)
        {
            XmlElement element = mDoc.CreateElement("AttackDefense");
            foreach (var atk in info.Attack)
            {
                XmlElement atkElement = mDoc.CreateElement("Attack");
                atkElement.SetAttribute("Name", mFormatter.AttrName(atk.Key));
                atkElement.SetAttribute("Percent", atk.Value.ToString());
                element.AppendChild(atkElement);
            }
            foreach (var def in info.Defense)
            {
                XmlElement defElement = mDoc.CreateElement("Defense");
                defElement.SetAttribute("Name", mFormatter.AttrName(def.Key));
                defElement.SetAttribute("Percent", def.Value.ToString());
                element.AppendChild(defElement);
            }
            if (element.HasChildNodes)
                parent.AppendChild(element);
        }

        void ProcessActions(ItemInfo info, XmlElement parent)
        {
            XmlElement element = mDoc.CreateElement("Actions");
            foreach (var act in info.Actions)
            {
                XmlElement actElement = mDoc.CreateElement("Action");
                actElement.SetAttribute("Name", mFormatter.Actions(act.Key));
                if (act.Value != null && act.Value.Count > 0)
                    ProcessRequirements(act.Value, actElement);
                element.AppendChild(actElement);
            }
            if (element.HasChildNodes)
                parent.AppendChild(element);
        }

        void ProcessEvents(ItemInfo info, XmlElement parent)
        {
            XmlElement element = mDoc.CreateElement("Events");
            foreach (var evt in info.Events)
            {
                XmlElement evtElement = mDoc.CreateElement("Event");
                evtElement.SetAttribute("Name", mFormatter.Events(evt.Key));
                foreach (var target in evt.Value)
                {
                    foreach (Function func in target.Value)
                    {
                        XmlElement funcElement = mDoc.CreateElement("Function");
                        funcElement.SetAttribute("Name", mFormatter.Functions(func.Key));
                        funcElement.SetAttribute("Target", mFormatter.Targets(func.Target));
                        funcElement.SetAttribute("TickCount", func.TickCount.ToString());
                        funcElement.SetAttribute("TickInterval", func.TickInterval.ToString());
                        XmlElement textElement = mDoc.CreateElement("Text");
                        string text;
                        try
                        {
                            text = ProcessFunctionArgs(func.Key, func.Arguments);
                        }
                        catch(InvalidCastException)
                        {
                            text = mFormatter.Functions(func.Key);
                            foreach (var arg in func.Arguments)
                            {
                                if (arg is byte[])
                                    text += " " + BitConverter.ToString((byte[])arg);
                                else
                                    text += " " + arg.ToString();
                            }
                        }
                        textElement.InnerXml = text;
                        funcElement.AppendChild(textElement);
                        if (func.Arguments != null && func.Arguments.Length > 0)
                        {
                            foreach (var arg in func.Arguments)
                            {
                                XmlElement argElement = mDoc.CreateElement("Argument");
                                if (arg is byte[])
                                    argElement.InnerText = BitConverter.ToString((byte[])arg);
                                else
                                    argElement.InnerText = arg.ToString();
                                funcElement.AppendChild(argElement);
                            }
                        }
                        if (func.Requirements != null && func.Requirements.Count > 0)
                        {
                            //XmlElement reqsElement = mDoc.CreateElement("Requirements");
                            ProcessRequirements(func.Requirements, funcElement);
                            //funcElement.AppendChild(reqsElement);
                        }
                        evtElement.AppendChild(funcElement);
                    }
                }
                element.AppendChild(evtElement);
            }
            if (element.HasChildNodes)
                parent.AppendChild(element);
        }

        string ProcessFunctionArgs(int key, object[] args)
        {
            switch ((FunctionKey)key)
            {
                case FunctionKey.Taunt_NPC:
                    return "Taunt\t" + Convert.ToInt32(args[0]);

                case FunctionKey.Add_Skill:
                    return string.Format("Add lockable skill {0} {1}", mFormatter.AttrName(Convert.ToInt32(args[1])), args[0]);

                case FunctionKey.Special_Hit:
                case FunctionKey.Hit:
                    int val0 = Convert.ToInt32(args[0]);
                    int val1 = Convert.ToInt32(args[1]);
                    switch ((AttributeKey)val0)
                    {
                        case AttributeKey.Credits:
                            if (val1 < 0)
                                return string.Format("Charge\t{0} credits.", mFormatter.Credits(val1));
                            else
                                return string.Format("Reward\t{0} credits.", mFormatter.Credits(val1));
                        case AttributeKey.MapUpgrades:
                            return string.Format("Add map upgrade\t{0}.", mFormatter.AttrValue(val0, val1));
                        case AttributeKey.MapFlags1:
                        case AttributeKey.MapFlags2:
                        case AttributeKey.MapFlags3:
                        case AttributeKey.MapFlags4:
                            return string.Format("Add map\t{0}.", mFormatter.AttrValue(val0, val1));
                        default:
                            bool damage = false;
                            int val2 = Convert.ToInt32(args[2]);
                            int val3 = Convert.ToInt32(args[3]);
                            string func = mFormatter.Functions(key);
                            string attr = mFormatter.AttrName(val0);
                            if (val1 < 0 && val2 <= 0)
                            {
                                val1 *= -1;
                                val2 *= -1;
                                damage = true;
                            }
                            string hit = val1.ToString();
                            if (val2 > val1)
                                hit += " - " + val2.ToString();
                            if (val2 == 0)
                                hit += '%';
                            if (val1 > 0 && !damage)
                            {
                                if (val3 > 0)
                                    hit += '\t' + val3.ToString() + " times";

                                switch ((AttributeKey)val0)
                                {
                                    case AttributeKey.Health:
                                        func = "";
                                        attr = "Healing";
                                        break;
                                    case AttributeKey.NanoCost:
                                        func = "";
                                        attr = "Nanopoints +";
                                        break;
                                }
                            }
                            else
                            {
                                if (val3 > 0)
                                    hit += '\t' + mFormatter.AttrName(val3);

                                switch ((AttributeKey)val0)
                                {
                                    case AttributeKey.Health:
                                        func = "";
                                        attr = "Damage";
                                        break;
                                    case AttributeKey.NanoCost:
                                        func = "";
                                        attr = "Nanopoints -";
                                        break;
                                }
                            }

                            return string.Format("{0}\t{1}\t{2}", func, attr, hit).Trim('\t', ' ');
                    }

                case FunctionKey.Set:
                    switch ((AttributeKey)Convert.ToInt32(args[0]))
                    {
                        case AttributeKey.ShadowBreedTemplate:
                            return string.Format("Set\t{0}\tto [item:{1}]", mFormatter.AttrName(Convert.ToInt32(args[0])), Convert.ToInt32(args[1]));
                        default:
                            return string.Format("Set\t{0}\tto {1}", mFormatter.AttrName(Convert.ToInt32(args[0])), args[1]);
                    }

                case FunctionKey.Skill:
                case FunctionKey.Modify:
                    switch ((AttributeKey)Convert.ToInt32(args[0]))
                    {
                        case AttributeKey.CriticalIncrease:
                        case AttributeKey.HealEfficiency:
                        case AttributeKey.NanoCost:
                        case AttributeKey.XPModifier:
                        case AttributeKey.Scale:
                            return string.Format("Modify\t{0}\t{1}%", mFormatter.AttrName(Convert.ToInt32(args[0])), args[1]);
                        default:
                            return string.Format("Modify\t{0}\t{1}", mFormatter.AttrName(Convert.ToInt32(args[0])), args[1]);
                    }

                case FunctionKey.Timed_Effect:
                    return string.Format("{0} temp set to\t{1}\tfor {2}", mFormatter.AttrName(Convert.ToInt32(args[0])), args[1], mFormatter.Time(Convert.ToInt32(args[2])));

                case FunctionKey.Change_Variable:
                    return string.Format("{0} temp set to\t{1}", mFormatter.AttrName(Convert.ToInt32(args[0])), args[1]);

                case FunctionKey.Lock_Skill:
                    return string.Format("{0} skill locked for {1}.", mFormatter.AttrName(Convert.ToInt32(args[0])), mFormatter.Time(Convert.ToInt32(args[1])));

                case FunctionKey.Head_Text:
                    return string.Format("Head Text:\t&quot;{0}&quot;\tfor {1}", Convert.ToString(args[0]), mFormatter.Time(Convert.ToInt32(args[1])));

                case FunctionKey.System_Text:
                case FunctionKey.System_Text_2:
                    return string.Format("System Text:\t&quot;{0}&quot;", Convert.ToString(args[0]));

                case FunctionKey.Cast_Nano:
                case FunctionKey.Cast_Nano_2:
                    return string.Format("Cast\t[nano:{0}]", Convert.ToInt32(args[0]));

                case FunctionKey.Team_Cast_Nano:
                    return string.Format("Cast\t[nano:{0}]\ton team.", Convert.ToInt32(args[0]));

                case FunctionKey.Pets_Cast_Nano:
                    return string.Format("Cast\t[nano:{0}]\ton pets.", Convert.ToInt32(args[0]));

                case FunctionKey.Area_Cast_Nano:
                    return string.Format("Cast\t[nano:{0}]\tin a {1}m radius.", Convert.ToInt32(args[0]), Convert.ToString(args[1]));

                case FunctionKey.Cast_Stun_Nano:
                    return string.Format("Cast stun\t[nano:{0}]\t{1}% chance.", Convert.ToInt32(args[0]), Convert.ToString(args[1]));

                case FunctionKey.Upload_Nano:
                    return string.Format("Upload\t[nano:{0}]", Convert.ToInt32(args[0]));

                case FunctionKey.Resist_Nano_Strain:
                    return string.Format("Resist\t{0}\t{1}%", mFormatter.NanoLines(Convert.ToInt32(args[0])), Convert.ToString(args[1]));

                case FunctionKey.Remove_Nano_Strain:
                    return string.Format("Remove all\t{0} nanos", mFormatter.NanoLines(Convert.ToInt32(args[0])));

                case FunctionKey.Reduce_Nano_Strain_Duration:
                    return string.Format("Reduce\t{0} nanos\tby {1}", mFormatter.NanoLines(Convert.ToInt32(args[0])), mFormatter.Time(Convert.ToInt32(args[1])));

                case FunctionKey.Spawn_Item:
                    string duration = mFormatter.Time(Convert.ToInt32(args[2]));
                    if (!string.IsNullOrEmpty(duration))
                        duration = "\tfor " + duration;
                    return string.Format("Spawn\tQL {0}\t{1}{2}", args[1], args[0], duration);

                case FunctionKey.Destroy_Item:
                    return "Destroy item.";

                case FunctionKey.Lock_Perk:
                    return string.Format("Lock\t[perk:{0}]\tfor {1}", Convert.ToInt32(args[1]), mFormatter.Time(Convert.ToInt32(args[2])));

                case FunctionKey.Add_Defense_Proc:
                    return string.Format("Defensive Proc\t[nano:{0}]\t({1}% chance to activate)", Convert.ToInt32(args[1]), args[0]);

                case FunctionKey.Add_Action:
                    return string.Format("Add Action\t[item:{0}]", Convert.ToInt32(args[3]));

                case FunctionKey.Set_Flag:
                    int attrKey = Convert.ToInt32(args[0]);
                    int attrVal = Convert.ToInt32(args[1]);
                    switch ((AttributeKey)attrKey)
                    {
                        case AttributeKey.MapUpgrades:
                            return string.Format("Add map upgrade\t{0}.", mFormatter.AttrValue(attrKey, attrVal));
                        case AttributeKey.MapFlags1:
                        case AttributeKey.MapFlags2:
                        case AttributeKey.MapFlags3:
                        case AttributeKey.MapFlags4:
                            return string.Format("Add map\t{0}.", mFormatter.AttrValue(attrKey, attrVal));
                        default:
                            return string.Format("Set Flag\t{0}\t{1}", mFormatter.AttrName(attrKey), mFormatter.AttrValue(attrKey, attrVal));
                    }

                case FunctionKey.Summon_Pet:
                    return "Warp targeted pet to current position.";

                case FunctionKey.Summon_Pets:
                    return "Warp all pets to current position.";

                case FunctionKey.Summon_Player:
                    return "Warp targeted team mate to current position.";

                case FunctionKey.Summon_Team_Mates:
                    return "Warp all team mates to current position.";

                case FunctionKey.Teleport:
                    return string.Format("Teleport to\t{0}", mFormatter.Playfields(Convert.ToInt32(args[3])));

                case FunctionKey.Teleport_Proxy:
                case FunctionKey.Teleport_Proxy_2:
                    return string.Format("Teleport to\t{0}", mFormatter.Playfields(Convert.ToInt32(args[1])));

                case FunctionKey.Teleport_To_Save_Point:
                    return "Teleport to\tLast save point";

                case FunctionKey.Set_Anchor:
                    return "Set Anchor";

                default:
                    {
                        string text = ItemFormatter.GetFunction(key);
                        if (args != null)
                        {
                            text += " Args( " + args.Length.ToString() + " ) ";
                            foreach (object arg in args)
                            {
                                if (arg is byte[])
                                    text += BitConverter.ToString((byte[])arg);
                                else
                                    text += Convert.ToString(arg);
                                text += ", ";
                            }
                            text = text.TrimEnd(' ', ',');
                        }
                        return text;
                    }
            }
        }

        void ProcessRequirements(RequirementList reqs, XmlElement parent)
        {
            foreach (Requirement req in reqs.Flatten())
            {
                XmlElement element = mDoc.CreateElement("Requirement");
                element.SetAttribute("Target", mFormatter.Targets(req.Target));
                element.SetAttribute("Stat", mFormatter.AttrName(req.Key));
                element.SetAttribute("Operator", mFormatter.Operators(req.Operator));
                if (req.Key == 0)
                    element.SetAttribute("Value", req.Value.ToString());
                else
                    element.SetAttribute("Value", mFormatter.AttrValue(req.Key, req.Value));
                if (req.ChildOp != -1)
                    element.SetAttribute("ChildOp", mFormatter.Operators(req.ChildOp));
                parent.AppendChild(element);
            }
        }

        void ProcessPerk(Perk perk, ItemStatus status)
        {
            if (perk == null || mTypeElement == null)
                return;

            XmlElement element = mDoc.CreateElement("Perk");
            element.SetAttribute("ID", perk.Id.ToString());
            element.SetAttribute("Template", perk.Template.ToString());
            element.SetAttribute("Parent", perk.Parent.ToString());
            for (int i = 0; i < perk.Params.Length; ++i)
                element.SetAttribute("Param_" + (i + 1), perk.Params[i].ToString());
            element.SetAttribute("Status", status.ToString());
            mTypeElement.AppendChild(element);
        }
    }
}
