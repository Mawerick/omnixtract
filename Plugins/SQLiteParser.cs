﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2018
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using OmniXtract.Lib;
using System;
using System.Data;
using System.IO;
using System.Linq;
using Twp.Data.AO;
using Twp.Utilities;

namespace OmniXtract.Plugins
{
    [PluginName("SQLite Item Parser")]
    public class SQLiteParser : IPlugin
    {
        public int[] Types
        {
            get
            {
                return new int[]
                {
                    ResourceType.Item,
                    ResourceType.Nano,
                    ResourceType.Perk,
                    ResourceType.Icon,
                    ResourceType.Textures_High,
                };
            }
        }

        #region SQL Scheme

        static readonly string CreateTables = @"
            CREATE TABLE localdb (
                key				TEXT NOT NULL,
                value			TEXT
            );
            CREATE TABLE android_metadata (
                locale			TEXT DEFAULT 'en_US'
            );
            CREATE TABLE items (
                _id				INTEGER NOT NULL PRIMARY KEY,
                name			TEXT NOT NULL,
                description		TEXT NOT NULL,
                type			INTEGER NOT NULL,
                icon			INTEGER,
                ql				INTEGER,
                status			INTEGER
            );
            CREATE TABLE items_attributes (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL
            );
            CREATE TABLE items_attack (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL
            );
            CREATE TABLE items_defense (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL
            );
            CREATE TABLE items_action_reqs (
                owner			INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                target			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL,
                operator		INTEGER,
                childOp			INTEGER
            );
            CREATE TABLE items_functions (
                owner			INTEGER NOT NULL,
                event			INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                target			INTEGER NOT NULL,
                tick_count		INTEGER,
                tick_interval	INTEGER,
                arg1			TEXT,
                arg2			TEXT,
                arg3			TEXT,
                arg4			TEXT,
                arg5			TEXT,
                arg6			TEXT,
                arg7			TEXT,
                arg8			TEXT,
                arg9			TEXT,
                arg10			TEXT,
                arg12			TEXT,
                arg11			TEXT,
                arg13			TEXT,
                arg14			TEXT,
                arg15			TEXT
            );
            CREATE TABLE items_function_reqs (
                owner			INTEGER NOT NULL,
                event			INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                function		INTEGER NOT NULL,
                target			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL,
                operator		INTEGER,
                childOp			INTEGER
            );
            CREATE TABLE items_anim_sets (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                data			BLOB
            );
            CREATE TABLE items_sound_sets (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                data			BLOB
            );

            CREATE TABLE nanos (
                _id				INTEGER NOT NULL PRIMARY KEY,
                name			TEXT NOT NULL,
                description		TEXT NOT NULL,
                type			INTEGER NOT NULL,
                icon			INTEGER,
                ql				INTEGER,
                status			INTEGER
            );
            CREATE TABLE nanos_attributes (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL
            );
            CREATE TABLE nanos_attack (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL
            );
            CREATE TABLE nanos_defense (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL
            );
            CREATE TABLE nanos_action_reqs (
                owner			INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                target			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                value			INTEGER NOT NULL,
                operator		INTEGER,
                childOp			INTEGER
            );
            CREATE TABLE nanos_functions (
                owner			INTEGER NOT NULL,
                event			INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                target			INTEGER NOT NULL,
                tick_count		INTEGER,
                tick_interval	INTEGER,
                arg1			TEXT,
                arg2			TEXT,
                arg3			TEXT,
                arg4			TEXT,
                arg5			TEXT,
                arg6			TEXT,
                arg7			TEXT,
                arg8			TEXT,
                arg9			TEXT,
                arg10			TEXT,
                arg11			TEXT,
                arg12			TEXT,
                arg13			TEXT,
                arg14			TEXT,
                arg15			TEXT
            );
            CREATE TABLE nanos_function_reqs (
                owner			INTEGER NOT NULL,
                event			INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                function		INTEGER NOT NULL,
                target			INTEGER NOT NULL,
                key  			INTEGER NOT NULL,
                value			INTEGER NOT NULL,
                operator		INTEGER,
                childOp			INTEGER
            );
            CREATE TABLE nanos_anim_sets (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                data			BLOB
            );
            CREATE TABLE nanos_sound_sets (
                owner			INTEGER NOT NULL,
                key				INTEGER NOT NULL,
                data			BLOB
            );

            CREATE TABLE perks (
                _id				INTEGER NOT NULL PRIMARY KEY,
                template		INTEGER NOT NULL,
                parent			INTEGER NOT NULL,
                param1			INTEGER NOT NULL,
                param2			INTEGER NOT NULL,
                param3			INTEGER NOT NULL,
                param4			INTEGER NOT NULL,
                status			INTEGER
            );
            CREATE TABLE icons (
                _id				INTEGER NOT NULL,
                bytes			BLOB NOT NULL,
                status			INTEGER
            );
            CREATE TABLE textures (
                _id				INTEGER NOT NULL,
                bytes			BLOB NOT NULL,
                status			INTEGER
            );";

        static readonly string CreateIndexes = @"
            CREATE INDEX idx_items ON items (_id, name);
            CREATE INDEX idx_items_action_reqs ON items_action_reqs (owner, parent, key, value, operator);
            CREATE INDEX idx_items_attack ON items_attack (owner, key, value);
            CREATE INDEX idx_items_attributes ON items_attributes (owner, key, value);
            CREATE INDEX idx_items_defense ON items_defense (owner, key, value);
            CREATE INDEX idx_items_functions ON items_functions (owner, event, parent, key, target);
            CREATE INDEX idx_items_function_reqs ON items_function_reqs (owner, event, target, parent, key, value, operator);
            CREATE INDEX idx_nanos ON nanos (_id, name);
            CREATE INDEX idx_nanos_action_reqs ON nanos_action_reqs (owner, parent, key, value, operator);
            CREATE INDEX idx_nanos_attack ON nanos_attack (owner, key, value);
            CREATE INDEX idx_nanos_attributes ON nanos_attributes (owner, key, value);
            CREATE INDEX idx_nanos_defense ON nanos_defense (owner, key, value);
            CREATE INDEX idx_nanos_functions ON nanos_functions (owner, event, parent, key, target);
            CREATE INDEX idx_nanos_function_reqs ON nanos_function_reqs (owner, event, target, parent, key, value, operator);
            CREATE INDEX idx_perks ON perks (_id, template, parent);
            CREATE INDEX idx_icons ON icons (_id);
            CREATE INDEX idx_textures ON textures(_id);";

        #endregion

        SQLiteDatabase mDatabase;
        string mFilename;
        string mTempfile;
        FunctionSets mSets;

        public void Initialize(string output, AOVersion version)
        {
            mFilename = output + ".db3";
            mTempfile = Path.Combine(Path.GetDirectoryName(mFilename), "temp.db3");
            if (File.Exists(mTempfile))
                File.Delete(mTempfile);

            mSets = new FunctionSets("FunctionSets.ini", Path.Combine(PathExt.GetExecPath(), "Config"), version);
            ItemParser.Trace = true;

            mDatabase = new SQLiteDatabase(mTempfile);
            mDatabase.Open();
            mDatabase.StartTransaction();
            Log.Debug("Creating tables");
            mDatabase.NonQuery(CreateTables);
            Log.Debug("Database ready");
        }

        public void Abort()
        {
            Log.Debug("Aborting...");
            mDatabase.RollbackTransaction();
            mDatabase.Close();
            File.Delete(mTempfile);
            ItemParser.Trace = false;
        }

        public void Cleanup()
        {
            Log.Debug("Creating indexes");
            mDatabase.NonQuery(CreateIndexes);
            Log.Debug("Committing transaction");
            mDatabase.CommitTransaction();
            mDatabase.Close();
            if (File.Exists(mFilename))
                File.Delete(mFilename);
            File.Move(mTempfile, mFilename);
            ItemParser.Trace = false;
        }

        public void ProcessRecord(int type, int id, byte[] data, ItemStatus status)
        {
            switch (type)
            {
                case ResourceType.Item:
                    var item = ItemParser.Parse(id, data, mSets);
                    ProcessItem(item, status);
                    break;
                case ResourceType.Nano:
                    var nano = ItemParser.Parse(id, data, mSets);
                    nano.Type = ItemType.Nano;
                    ProcessItem(nano, status);
                    break;
                case ResourceType.Perk:
                    ProcessPerk(new Perk(id, data), status);
                    break;
                case ResourceType.Icon:
                    ProcessImage(true, id, data, status);
                    break;
                case ResourceType.Textures_High:
                    ProcessImage(false, id, data, status);
                    break;
                default:
                    Log.Debug("Unknown type: " + type);
                    break;
            }
        }

        void ProcessItem(ItemInfo info, ItemStatus status)
        {
            if (info == null)
                return;

            string table = "items";
            if (info.Type == ItemType.Nano)
                table = "nanos";

            var args = new SQLiteArgument[] {
                new SQLiteArgument("_id", DbType.Int32, info.Id),
                new SQLiteArgument("name", DbType.String, info.Name),
                new SQLiteArgument("description", DbType.String, info.Description),
                new SQLiteArgument("type", DbType.Int32, (int) info.Type),
                new SQLiteArgument("ql", DbType.Int32, info.QL),
                new SQLiteArgument("status", DbType.Int32, (int)status)
            };
            mDatabase.Insert(table, args);

            // Process Attributes
            foreach (var attribute in info.Attributes)
            {
                args = new SQLiteArgument[] {
                    new SQLiteArgument("owner", DbType.Int32, info.Id),
                    new SQLiteArgument("key", DbType.Int32, attribute.Key),
                    new SQLiteArgument("value", DbType.Int32, attribute.Value)
                };
                mDatabase.Insert(table + "_attributes", args);
            }

            // Process attack/defense
            foreach (var attack in info.Attack)
            {
                args = new SQLiteArgument[] {
                    new SQLiteArgument("owner", DbType.Int32, info.Id),
                    new SQLiteArgument("key", DbType.Int32, attack.Key),
                    new SQLiteArgument("value", DbType.Int32, attack.Value),
                };
                mDatabase.Insert(table + "_attack", args);
            }
            foreach (var defense in info.Defense)
            {
                args = new SQLiteArgument[] {
                    new SQLiteArgument("owner", DbType.Int32, info.Id),
                    new SQLiteArgument("key", DbType.Int32, defense.Key),
                    new SQLiteArgument("value", DbType.Int32, defense.Value),
                };
                mDatabase.Insert(table + "_defense", args);
            }

            // Process actions
            foreach (var action in info.Actions)
            {
                foreach (var req in action.Value.Flatten())
                {
                    args = new SQLiteArgument[] {
                        new SQLiteArgument("owner", DbType.Int32, info.Id),
                        new SQLiteArgument("parent", DbType.Int32, action.Key),
                        new SQLiteArgument("target", DbType.Int32, req.Target),
                        new SQLiteArgument("key", DbType.Int32, req.Key),
                        new SQLiteArgument("value", DbType.Int32, req.Value),
                        new SQLiteArgument("operator", DbType.Int32, req.Operator),
                        new SQLiteArgument("childOp", DbType.Int32, req.ChildOp),
                    };
                    mDatabase.Insert(table + "_action_reqs", args);
                }
            }

            // Process events
            foreach (var evt in info.Events)
            {
                ProcessEvent(info.Id, evt.Key, evt.Value, table);
            }

            // Process animation/audio sets
            foreach (var anim in info.AnimSets)
            {
                args = new SQLiteArgument[] {
                    new SQLiteArgument("owner", DbType.Int32, info.Id),
                    new SQLiteArgument("key", DbType.Int32, anim.Key),
                    new SQLiteArgument("data", DbType.Binary, IntArray.ToByteArray(anim.AnimList))
                };
                mDatabase.Insert(table + "_anim_sets", args);
            }
            foreach (var sound in info.SoundSets)
            {
                args = new SQLiteArgument[] {
                    new SQLiteArgument("owner", DbType.Int32, info.Id),
                    new SQLiteArgument("key", DbType.Int32, sound.Key),
                    new SQLiteArgument("data", DbType.Binary, IntArray.ToByteArray(sound.AnimList))
                };
                mDatabase.Insert(table + "_sound_sets", args);
            }
        }

        void ProcessEvent(int owner, int key, FunctionList functions, string table)
        {
            foreach (var target in functions)
            {
                int index = 0;
                foreach (var function in target.Value)
                {
                    var args = new SQLiteArgument[]
                    {
                        new SQLiteArgument("owner", DbType.Int32, owner),
                        new SQLiteArgument("event", DbType.Int32, key),
                        new SQLiteArgument("parent", DbType.Int32, target.Key),
                        new SQLiteArgument("key", DbType.Int32, function.Key),
                        new SQLiteArgument("target", DbType.Int32, function.Target),
                        new SQLiteArgument("tick_count", DbType.Int32, function.TickCount),
                        new SQLiteArgument("tick_interval", DbType.Int32, function.TickInterval)
                    };
                    if (function.Arguments != null)
                    {
                        var list = args.ToList();
                        for (int i = 0; i < function.Arguments.Length; ++i)
                            list.Add(new SQLiteArgument("arg" + (i + 1), DbType.String, Convert.ToString(function.Arguments[i])));
                        args = list.ToArray();
                    }
                    mDatabase.Insert(table + "_functions", args.ToArray());

                    foreach (var req in function.Requirements.Flatten())
                    {
                        args = new SQLiteArgument[]
                        {
                            new SQLiteArgument("owner", DbType.Int32, owner),
                            new SQLiteArgument("event", DbType.Int32, key),
                            new SQLiteArgument("parent", DbType.Int32, target.Key),
                            new SQLiteArgument("function", DbType.Int32, index),
                            new SQLiteArgument("target", DbType.Int32, req.Target),
                            new SQLiteArgument("key", DbType.Int32, req.Key),
                            new SQLiteArgument("value", DbType.Int32, req.Value),
                            new SQLiteArgument("operator", DbType.Int32, req.Operator),
                            new SQLiteArgument("childOp", DbType.Int32, req.ChildOp)
                        };
                        mDatabase.Insert(table + "_function_reqs", args);
                    }
                    index++;
                }
            }
        }

        void ProcessPerk(Perk perk, ItemStatus status)
        {
            if (perk == null)
                return;

            var args = new SQLiteArgument[] {
                new SQLiteArgument("_id", DbType.Int32, perk.Id),
                new SQLiteArgument("template", DbType.UInt32, perk.Template),
                new SQLiteArgument("parent", DbType.Int32, perk.Parent),
                new SQLiteArgument("param1", DbType.Int32, perk.Params[0]),
                new SQLiteArgument("param2", DbType.Int32, perk.Params[1]),
                new SQLiteArgument("param3", DbType.Int32, perk.Params[2]),
                new SQLiteArgument("param4", DbType.Int32, perk.Params[3]),
                new SQLiteArgument("status", DbType.String, status.ToString()),
            };
            mDatabase.Insert("perks", args);
        }

        void ProcessImage(bool isIcon, int id, byte[] data, ItemStatus status)
        {
            if (data == null)
                return;

            string table = "textures";
            if (isIcon)
                table = "icons";

            var args = new SQLiteArgument[]
            {
                new SQLiteArgument("_id", DbType.Int32, id),
                new SQLiteArgument("bytes", DbType.Binary, data),
                new SQLiteArgument("status", DbType.Int32, (int)status),
            };
            mDatabase.Insert(table, args);
        }
    }
}
