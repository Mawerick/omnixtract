﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using Twp.Data.AO;

namespace OmniXtract.Lib
{
    /// <summary>
    /// The plugin interface.
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Gets an array containing the data types this plugin supports.
        /// </summary>
        int[] Types { get; }

        /// <summary>
        /// This gets called when we start
        /// </summary>
        /// <param name="output">A string representing the path and filename where the extracted file should be stored.</param>
        /// <param name="version">The version of the source data.</param>
        /// <returns>true if it's ok to begin extracting, otherwise; false</returns>
        void Initialize(string output, AOVersion version);

        /// <summary>
        /// This gets called when we are done.
        /// </summary>
        void Cleanup();

        /// <summary>
        /// Gets called when an error has occurd, allowing the plugin to do some cleanup.
        /// </summary>
        void Abort();
        
        /// <summary>
        /// This gets called when a data record has been extracted, and is ready for processing by the plugin.
        /// </summary>
        /// <param name="type">The record type.</param>
        /// <param name="id">The record id.</param>
        /// <param name="data">The record data.</param>
        /// <param name="status">The status of the record, when comparing two versions.</param>
        void ProcessRecord(int type, int id, byte[] data, ItemStatus status);
    }
}
