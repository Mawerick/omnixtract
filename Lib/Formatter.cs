﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Twp.Utilities;

namespace OmniXtract.Lib
{
    public sealed class Formatter
    {
        public Formatter()
        {
            string path = Path.Combine(PathExt.GetExecPath(), "Config");
            foreach (string table in mTables)
            {
                string fileName = table + ".ini";
                IniDocument doc = new IniDocument(fileName, path);
                doc.Read();
                if (doc.Sections.Count > 0)
                    mFiles.Add(table, doc);
            }
        }

        SortedList<string, IniDocument> mFiles = new SortedList<string, IniDocument>();
        static readonly string[] mTables =
        {
            "Actions",
            "ArmorSlots",
            "AttrNames",
            "AttrValues",
            "CanFlags",
            "Events",
            "ExpacFlags",
            "Flags",
            "Functions",
            "ImplantSlots",
            "ItemTypes",
            "NanoFlags",
            "NanoLines",
            "Operators",
            "Playfields",
            "Targets",
            "WeaponSlots"
        };

        bool mDefined = false;
        List<string> mTracer = new List<string>();

        public bool Defined
        {
            get { return mDefined; }
        }

        public string[] Trace
        {
            get { return mTracer.ToArray(); }
        }

        public string Actions(int actionNum)
        {
            mTracer.Clear();
            return Lookup("Actions", actionNum);
        }

        public string ArmorSlots(int slotFlags)
        {
            mTracer.Clear();
            return BitFlags("ArmorSlots", slotFlags);
        }

        public string AttrName(int attrNum)
        {
            mTracer.Clear();
            return Lookup("AttrNames", attrNum);
        }

        public string AttrValue(int attrNum, int attrVal, int equipPage = 0)
        {
            mTracer.Clear();
            string table = "";
            switch (attrNum)
            {
                case 88:
                case 298:
                    if (attrNum == 88)
                        attrVal = (0x1 << attrVal);
                    //attrVal = attrVal & (int)0xfffffffe;
                    switch (equipPage)
                    {
                        case 0:
                            return "0";
                        case 1:
                            table = "WeaponSlots";
                            break;
                        case 2:
                        case 4:
                            table = "ArmorSlots";
                            break;
                        case 3:
                        case 5:
                            table = "ImplantSlots";
                            break;
                        default:
                            table = "EquipPage_" + equipPage;
                            break;
                    }
                    return BitFlags(table, attrVal);

                case 8:     // TimeExists
                case 210:   // RechargeDelay
                case 211:   // EquipDelay
                case 294:   // AttackDelay
                case 523:   // AttackDelayCap
                case 524:   // RechargeDelayCap
                    return (attrVal / 100.0).ToString(CultureInfo.InvariantCulture);

                case 2:     // VolumeMass
                case 24:    // MaxMass
                case 78:    // CurrentMass
                    return (attrVal / 1000.0).ToString(CultureInfo.InvariantCulture);
            }

            if (LookupInternal("AttrValues", "BitFlags", attrNum.ToString()) == "True")
                return BitFlags("AttrValues", attrVal, attrNum);
            else
                return Lookup("AttrValues", attrVal, attrNum);
        }

        public string CanFlags(int flagBits)
        {
            mTracer.Clear();
            return BitFlags("CanFlags", flagBits);
        }

        public string Events(int eventNum)
        {
            mTracer.Clear();
            return Lookup("Events", eventNum);
        }

        public string ExpacFlags(int flagBits)
        {
            mTracer.Clear();
            return BitFlags("ExpacFlags", flagBits);
        }

        public string Flags(int flagBits)
        {
            mTracer.Clear();
            return BitFlags("Flags", flagBits);
        }

        public string Functions(int funcNum)
        {
            mTracer.Clear();
            return Lookup("Functions", funcNum);
        }

        public string ImplantSlots(int slotFlags)
        {
            mTracer.Clear();
            return BitFlags("ImplantSlots", slotFlags);
        }

        public string NanoFlags(int flagBits)
        {
            mTracer.Clear();
            return BitFlags("NanoFlags", flagBits);
        }

        public string NanoLines(int line)
        {
            mTracer.Clear();
            return Lookup("NanoLines", line);
        }

        public string Operators(int opNum)
        {
            mTracer.Clear();
            return Lookup("Operators", opNum);
        }

        public string Playfields(int pfNum)
        {
            mTracer.Clear();
            string pf = pfNum.ToString();
            string name = LookupInternal("Playfields", pf, "-1");
            if (name == pf)
            {
                string[] ranges = LookupInternal("Playfields", pf, "1").Split(' ');
                foreach(string range in ranges)
                {
                    if (range.Contains("-"))
                    {
                        string[] vals = range.Split('-');
                        int min = Convert.ToInt32(vals[0]);
                        int max = Convert.ToInt32(vals[1]);
                        if (pfNum >= min && pfNum <= max)
                            return "Indoors Playfield";
                    }
                    else
                    {
                        int val = Convert.ToInt32(range);
                        if (pfNum == val)
                            return "Indoors Playfield";
                    }
                }

                ranges = LookupInternal("Playfields", pf, "2").Split(' ');
                foreach (string range in ranges)
                {
                    if (range.Contains("-"))
                    {
                        string[] vals = range.Split('-');
                        int min = Convert.ToInt32(vals[0]);
                        int max = Convert.ToInt32(vals[1]);
                        if (pfNum >= min && pfNum <= max)
                            return "City Backyard";
                    }
                    else
                    {
                        int val = Convert.ToInt32(range);
                        if (pfNum == val)
                            return "City Backyard";
                    }
                }
            }
            return name;
        }

        public string Targets(int target)
        {
            mTracer.Clear();
            return Lookup("Targets", target);
        }

        public string WeaponSlots(int slotFlags)
        {
            mTracer.Clear();
            return BitFlags("WeaponSlots", slotFlags);
        }

        private string BitFlags(string table, int value, int section = -1)
        {
            List<string> list = new List<string>();
            foreach (int bit in Flag.GetBits(value))
            {
                list.Add(Lookup(table, bit, section));
            }
            return string.Join(", ", list.ToArray());
        }

        public bool IsDefined(string table, int key, int section = -1)
        {
            Lookup(table, key, section);
            return Defined;
        }

        public string Lookup(string table, int key, int section = -1)
        {
            mTracer.Add(string.Format("[Formatter.Lookup] Table: {0} Section: {1} Key: {2}", table, section, key));
            try
            {
                return LookupInternal(table, key.ToString(), section.ToString());
            }
            catch (Exception ex)
            {
                mTracer.Add("Formatter: Lookup failed: " + ex.Message);
                return key.ToString();
            }
        }

        private string LookupInternal(string table, string key, string section)
        {
            int xrefCount = 0;
            mDefined = false;

        XRef:
            if (!mFiles.ContainsKey(table))
            {
                mTracer.Add("Error! Table " + table + " does not exist!");
                return key;
            }
            IniDocument doc = mFiles[table];

            if (!doc.Sections.ContainsKey(section))
            {
                mTracer.Add("Error! Section " + section + " does not exist in table " + table + "!");
                return key;
            }
            IniSection sect = doc[section];

            if (sect.ContainsKey("XREF"))
            {
                if (++xrefCount > 10)
                {
                    mTracer.Add("Maximum Cross-Reference limit reached!");
                    return key;
                }

                string xref = ParseValue(sect["XREF"]);
                mTracer.Add("[Formatter.Lookup] Cross-Reference: " + xref);
                string[] parts = xref.Split(';');
                if (parts.Length != 2)
                {
                    mTracer.Add("Error! Invalid Cross-Reference: " + xref + " (Valid format: <table>;<section>)");
                    return key;
                }

                table = parts[0];
                section = parts[1];
                goto XRef;
            }

            if (sect.ContainsKey(key))
            {
                mDefined = true;
                return ParseValue(sect[key]);
            }
            else
            {
                mTracer.Add("Error! Key " + key + " does not exist in " + table + ":" + section + "!");
                return key;
            }
        }

        private string ParseValue(string value)
        {
            List<string> list = new List<string>();
            string[] parts = QuotedString.Split(value, ' ');
            foreach(string part in parts)
            {
                list.Add(part.Trim('"').Trim());
            }
            return string.Join(" ", list.ToArray());
        }

        public string Credits(int credits)
        {
            if (credits < 0)
                credits *= -1;

            return credits.ToString("### ### ### ###").Trim();
        }

        public string Time(int seconds)
        {
            return Time(seconds, "#0");
        }

        public string Time(double seconds)
        {
            return Time(seconds, "#0.00");
        }

        public string Time(double seconds, string format)
        {
            string text = string.Empty;
            if (seconds > 60)
            {
                double minutes = Math.Floor(seconds / 60);
                seconds -= minutes * 60;

                if (minutes > 60)
                {
                    double hours = Math.Floor(minutes / 60);
                    minutes -= hours * 60;

                    text += hours.ToString("##0") + "h ";
                }
                if (minutes > 0)
                    text += minutes.ToString("#0") + "m ";
            }
            if (seconds > 0)
                text += seconds.ToString(format, System.Globalization.CultureInfo.InvariantCulture) + "s";
            return text.Trim();
        }
    }
}
