﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2015
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("OmniXtract")]
[assembly: AssemblyVersion("1.0.0.*")]

[assembly: AssemblyTitle("OmniXtract Library")]
[assembly: AssemblyDescription("A simple resource extraction tool for Anarchy Online.")]
[assembly: AssemblyCompany("WrongPlace.Net")]
[assembly: AssemblyCopyright("Mawerick, WrongPlace.Net 2015")]

[assembly: NeutralResourcesLanguage("en", UltimateResourceFallbackLocation.MainAssembly)]

[assembly: ComVisible(false)]
//[assembly: CLSCompliant(true)]

[assembly: Guid("66fc5b33-f944-4adb-80a6-f166810ac076")]
