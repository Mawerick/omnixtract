﻿=========
AOExtract
=========

AOExtract is an Anarchy Online Resource Database extraction tool.


Features:
=========

 - Extracts data...


Usage:
======

 - Start
 - Select AO path
 - Select output path
 - Select extraction plugin
 - Select data types to extract
 - Click Extract
 - Wait for the extraction to complete
 - Click Open output
 - ...
 - Profit?

Installation:
=============

Installer:
 - Download the file "AOExtract-1.0.0.exe" to wherever you store your stuff.
 - Run the installer and follow the instructions given.

Manual installation:
 - Download the file "AOExtract-1.0.0.zip" to wherever you store your stuff.
 - Extract the content to a desired location (e.g. "C:\Program Files\AOExtract").
 - (Optional) Create a shortcut to the file "AOExtract.exe" on your desktop.


Links:
======

 http://www.wrongplace.net/            News site
 http://www.anarchy-online.com/        Official Anarchy Online site

Credits:
========

Author: Mawerick (mawerick@wrongplace.net)
(Please write in english only)
